import logging
import os


class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_secret')
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True
    MONGO_URI = 'mongodb://' + os.getenv('MONGO_SERVER', 'localhost:27017/finance')

    logging.basicConfig(
        level=logging.INFO,
        filename='logger.log',
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        filemode='w'
    )


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    MONGO_URI = 'mongodb://' + os.getenv('MONGO_SERVER', 'localhost:27017/finance-test')

    logging.basicConfig(
        level=logging.INFO,
        filename='test_logger.log',
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        filemode='w'
    )


class ProductionConfig(Config):
    DEBUG = False
    MONGO_URI = 'mongodb://' + os.getenv('MONGO_SERVER', '')

    logging.basicConfig(
        level=logging.INFO,
        filename='logger.log',
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        filemode='a'
    )


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY
