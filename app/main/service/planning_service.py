import logging

from app.main.conversor.planning_conversor import convert_data_to_planning
from app.main.conversor.launch_conversor import dict_to_launch
from app.main.service import service
from app.main.dao import planning_dao
from app.main.util.diff import get_diff_values_dict

log = logging.getLogger(__name__)


def save_planning(data):
    planning = convert_data_to_planning(data)
    planning_dao.create_planning(planning)

    log.info('Planning created: {}'.format(data))
    response_object = {
        'status': 'success',
        'message': 'Planning category successfully created',
        'id': str(planning.id)
    }
    return response_object, 201


def update_planning(data):
    planning = convert_data_to_planning(data)
    planning_dao.update_planning(planning)

    log.info('Planning updated: {}'.format(data))
    response_object = {'status': 'success', 'message': 'Planning category successfully updated'}
    return response_object, 201


def list_planning(month_year):
    return planning_dao.get_plannings_by(month_year)


def add_launch(data):
    launch = dict_to_launch(data)
    launch.is_planning = True

    log.info('Launch added: {}'.format(data))
    response_object = {'status': 'success', 'message': 'Launch successfully registered on planning',
                       'id': str(launch.id)}
    return response_object, 201


def update_launch(data):
    new_launch = dict_to_launch(data)
    old_launch = planning_dao.get_launch(new_launch.setting.launch_id)
    values = get_diff_values_dict(old_launch, new_launch, root_replace='launches.$[launch].')
    service.update_series(old_launch, new_launch, values,
                          callable_get_launch_series=planning_dao.get_launches_series,
                          callable_persist_update_launch_date=planning_dao.persist_update_launch_date,
                          callable_remove_launches=planning_dao.remove_launches_by_index,
                          callable_persist_new_launches=planning_dao.persist_launches,
                          callable_update_series_installments=planning_dao.update_amount_installments_and_value)
    planning_dao.persist_changes(new_launch, values)

    log.info('Launch updated: {}'.format(data))
    response_object = {'status': 'success', 'message': 'Launch successfully updated'}
    return response_object, 201
