import logging
from typing import List

from app.main.conversor import category_conversor
from app.main.model.launch import Launch
from app.main.service import launch_service
from app.main.service.transformer import category_transformer
from app.main.dao import category_dao
from app.main.service.dto.response.category.categorized_launches_response import CategorizedLaunchesResponse

log = logging.getLogger(__name__)


def create_category(data):
    category = category_conversor.convert_data_to_category(data)
    category_dao.insert_category(category)

    log.info('Category created: {}'.format(vars(category)))
    response_object = {'status': 'success', 'message': 'Category successfully registered', 'id': str(category.id)}
    return response_object, 201


def update_category(data):
    category = category_conversor.convert_data_to_category(data)
    category_dao.update_category(category)

    log.info('Category updated: {}'.format(vars(category)))
    response_object = {'status': 'success', 'message': 'Category successfully updated'}
    return response_object, 201


def list_from_month_year(month_year: str, show_empty_categories: bool = False) -> List[CategorizedLaunchesResponse]:
    launches = launch_service.launches_by_monty_year(month_year)
    categories_id = _extract_categories(launches)

    categories = category_dao.find_by_list_ids(categories_id, show_empty_categories)
    return category_transformer.transform_to_categorized_launches_response(categories, launches)


def _extract_categories(launches: List[Launch]) -> List[str]:
    categories = map(lambda launch: launch.category_id, launches)
    return list(dict.fromkeys(categories))
