import logging
from datetime import date
from typing import List

from bson import ObjectId
from dateutil.relativedelta import relativedelta

from app.main.conversor import launch_conversor
from app.main.dao import launch_dao as dao
from app.main.model.launch import Launch, Setting

log = logging.getLogger(__name__)

def find_launch_by_id(launch_id: str) -> Launch:
    return dao.find_by_id(launch_id)

def launches_by_monty_year(month_year: str) -> List[Launch]:
    return dao.get_launches_by_month_year(month_year)

def generate_launches(data):
    launch = launch_conversor.dict_to_launch(data)
    launches = {
        'unique': _generate_unique,
        'repeat': _generate_repeat,
        'parcel': _generate_parcel
    }.get(launch.setting.option_frequency, lambda: SystemError('Function not found!'))(launch)

    dao.save_launch(launch.category_id, launches)

    response_object = {'status': 'success', 'message': 'Launch successfully added', 'id': str(launch.id)}
    return response_object, 201

def _generate_unique(launch: Launch) -> List[Launch]:
    launch.setting.launch_id = launch.id
    return [launch]

def _generate_repeat(launch: Launch) -> List[Launch]:
    launches = [Launch(
        _id=ObjectId(),
        description=launch.description,
        value=launch.value,
        status=launch.status,
        category_id=launch.category_id,
        date=add_month(launch.date, current_parcel),
        setting=Setting(
            ObjectId(),
            launch.id,
            launch.setting.frequency,
            launch.setting.option_frequency,
            current_installment=current_parcel + 1,
            total_installments=launch.setting.total_installments
        )
    ) for current_parcel in range(launch.setting.total_installments)]
    launch.setting.launch_id = launch.id
    launches[0] = launch
    return launches

def add_month(str_date, num_months):
    new_date = date.fromisoformat(f'{str_date}-01') + relativedelta(months=num_months)
    return new_date.strftime('%Y-%m')

def _generate_parcel(launch: Launch) -> List[Launch]:
    launch.value = round(launch.value / launch.setting.total_installments, 2)
    return _generate_repeat(launch)

def update_launch(data):
    launch, is_update_all = launch_conversor.dict_to_launch_update(data)
    update = {
        'unique': _update_unique,
        'repeat': _update_repeat,
        'parcel': _update_parcel
    }.get(launch.setting.option_frequency)
    if update is None:
        raise SystemError('Function not found!')
    
    update(launch, is_update_all)

    response_object = {'status': 'success', 'message': 'Launch successfully updated', 'id': str(launch.id)}
    return response_object, 200


def _update_unique(launch: Launch, is_update_all: bool):
    if is_update_all:
        dao.update_all(launch)
    elif launch.id == launch.setting.launch_id: # Unique without replication
        launch = Launch(
            _id=ObjectId(),
            description=launch.description,
            value=launch.value,
            status=launch.status,
            category_id=launch.category_id,
            date=launch.date if launch.date is not None else date.today().strftime('%Y-%m'),
            setting=launch.setting
        )
        dao.save_launch(launch.category_id, [launch])
    else:
        dao.update_launch(launch)
        

def _update_repeat(launch: Launch, is_update_all: bool):
    dao.update_all(launch) if is_update_all else dao.update_launch(launch)

def _update_parcel(launch: Launch, is_update_all: bool):
    dao.update_all(launch) if is_update_all else dao.update_launch(launch)

def remove_launch(launch_id: str):
    log.info(f'Finding launch id: ${launch_id}')
    launch = dao.find_by_id(launch_id)

    remove = {
        'unique': remove_launch_unique,
        'parcel': remove_launch_parcel,
        'repeat': remove_launch_repeat
    }.get(launch.setting.option_frequency)
    if remove is None:
        raise SystemError('Function not found!')

    remove(launch)

    log.info('Launch removed: {}'.format(launch_id))
    response_object = {'status': 'success', 'message': 'Launch successfully removed'}
    return response_object, 200

def remove_launch_repeat(launch: Launch):
    dao.remove_launch_series(launch)

def remove_launch_parcel(launch: Launch):
    dao.remove_launch_series(launch)

def remove_launch_unique(launch: Launch):
    dao.remove_launch_by_id(launch)

def _raise(exception):
    raise exception
