from functools import reduce
from typing import List

from app.main.model.category import Category
from app.main.model.launch import Launch
from app.main.service.dto.response.category.categorized_launches_response import CategorizedLaunchesResponse


def transform_to_categorized_launches_response(categories: List[Category], launches: List[Launch]) -> List[CategorizedLaunchesResponse]:
    return [
        CategorizedLaunchesResponse(
            category.id,
            category.title,
            category.color,
            category.category_type,
            category.planning,
            category.position,
            launches=[
                launch
                for launch in launches
                if launch.category_id == category.id
            ]
        )
        for category in categories
    ]
