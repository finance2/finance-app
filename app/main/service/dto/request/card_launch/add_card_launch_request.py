import dataclasses
import datetime


@dataclasses.dataclass
class Setting:
    frequency: str
    option_frequency: str
    current_installment: int
    total_installments: int


@dataclasses.dataclass
class AddCardLaunchRequest:
    description: str
    value: float
    category: str
    date: str
    card_id: str
    setting: Setting
