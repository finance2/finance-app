import dataclasses


@dataclasses.dataclass
class Setting:
    launch_id: str
    frequency: str
    option_frequency: str
    current_installment: int
    total_installments: int


@dataclasses.dataclass
class CardLaunchResponse:
    id: str
    description: str
    value: float
    category: str
    date: str
    card_id: str
    setting: Setting
