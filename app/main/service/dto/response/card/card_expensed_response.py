import dataclasses


@dataclasses.dataclass
class CardExpensedResponse:
    card_id: str
    expensed: float
