import dataclasses


@dataclasses.dataclass()
class CardResponse:
    id: str
    flag: str
    title: str
    owner: str
    colors: str
    number: str
    expensed: float
    planning: float = 0.0
