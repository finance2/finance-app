from typing import List


class CategorizedLaunchesResponse:
    def __init__(self, id, title, color, type, planning, position, launches):
        self.id = id
        self.title = title
        self.color = color
        self.type = type
        self.planning = planning
        self.position = position
        self.launches: List[LaunchResponse] = launches

class LaunchResponse:
    def __init__(self, id, description, value, status, category_id, date, setting):
        self.id = id
        self.description = description
        self.value = value
        self.status = status
        self.category_id = category_id
        self.date = date
        self.setting: SettingResponse = setting

class SettingResponse:
    def __init__(self, _id, launch_id, frequency, option_frequency, current_installment, total_installments):
        self._id = _id
        self.launch_id = launch_id
        self.frequency = frequency
        self.option_frequency = option_frequency
        self.current_installment = current_installment
        self.total_installments = total_installments
