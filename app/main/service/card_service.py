import logging
from typing import List

from app.main.conversor import (
    card_transformer as transformer,
    card_launch_transformer as card_launch_transformer
)
from app.main.dao import (
    card_dao as repository,
    card_launch_dao as launch_repository
)
from app.main.model.card import Card
from app.main.service.dto.request.card_launch.add_card_launch_request import AddCardLaunchRequest
from app.main.service.dto.response.card.card_expensed_response import CardExpensedResponse
from app.main.service.dto.response.card.card_response import CardResponse
from app.main.service.dto.response.card_launch.card_launch_response import CardLaunchResponse

log = logging.getLogger(__name__)


def create_card(data) -> str:
    log.info(f'Creating card from request: {data}')
    card = transformer.from_create_card_request(data)
    repository.save_card(card)

    return str(card.id)


def update_card(data):
    log.info(f'Updating card from request: {data}')
    card = transformer.from_update_card_request(data)
    repository.update_card(card)


def find_card_by_id(card_id: str) -> CardResponse:
    log.info(f'Searching card with id {card_id}')
    card = repository.card_by_id(card_id)

    return transformer.from_card_model(card)


def list_cards(month_year) -> [CardResponse]:
    log.info('Finding all cards')
    cards = repository.list_cards()
    expensed_cards = launch_repository.find_total_expensed_cards_by(month_year)

    __fill_expensed_values(cards, expensed_cards)

    return list(map(transformer.from_card_model, cards))


def __fill_expensed_values(cards: List[Card], cards_expensed: List[CardExpensedResponse]):
    log.info(f'Fill expensed values on cards')
    keys = list(map(lambda expense: expense.card_id, cards_expensed))
    for card in cards:
        try:
            index = keys.index(str(card.id))
            keys.pop(index)
            card.expensed = cards_expensed.pop(index).expensed
        except ValueError:
            log.warning(f'Card {card.id} has no expense')
            card.expensed = 0


def find_card_launches_by_month(month_year: str, card_id: str) -> List[CardLaunchResponse]:
    log.info(f'Finding launches by month {month_year}')
    card_launches = launch_repository.find_cards_launch_by_month(month_year, card_id)

    return list(map(card_launch_transformer.to_card_launch_response, card_launches))


def add_card_launch(data):
    log.info(f'Adding card launch from request: {data}')
    request = card_launch_transformer.to_add_card_launch_request(data)

    return _add_unique_launch(request)\
        if request.setting.option_frequency == 'unique'\
        else _add_many_launches(request)


def _add_unique_launch(request: AddCardLaunchRequest):
    log.info(f'Adding unique launch {request}')
    card_launch = card_launch_transformer.from_add_card_launch_request(request)

    launch_repository.save_card_launch([card_launch])
    return str(card_launch.id)


def _add_many_launches(request: AddCardLaunchRequest):
    log.info(f'Adding repeat launch {request}')
    launches = card_launch_transformer.generate_repeated_launches(request)

    launch_repository.save_card_launch(launches)
    return str(launches[0].id)


def update_launch_card(data: dict):
    log.info(f'Updating launch {data}')
    card_launch = card_launch_transformer.to_update_card_launch_request(data)

    launch_repository.update_card_launch(card_launch)


def remove_launch_card(launch_id: str):
    log.info(f'Removing series launch with id {launch_id}')
    launch_repository.remove_card_launch(launch_id)


def remove_card(card_id: str):
    log.info(f'Removing card with id {card_id}')
    launch_repository.remove_card_launches_by_card(card_id)
    repository.remove_card(card_id)
