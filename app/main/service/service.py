import calendar
from datetime import date
from typing import Callable, List

from bson import ObjectId
from dateutil.relativedelta import relativedelta

from app.main.model.launch import Launch, Setting
from app.main.util.dict_converter import convert_to_dict


def update_series(old_value: Launch, new_value, diff_values,
                  callable_get_launch_series: Callable[[ObjectId], List[Launch]],
                  callable_persist_update_launch_date: Callable[[Launch], None],
                  callable_remove_launches: Callable[[Launch, int], None],
                  callable_persist_new_launches: Callable[[ObjectId, List[Launch]], None],
                  callable_update_series_installments: Callable[[Launch, float, int], None]):
    update_date_series(old_value, diff_values, callable_get_launch_series, callable_persist_update_launch_date)
    update_installments_series(old_value, new_value, diff_values,
                               callable_remove_launches,
                               callable_persist_new_launches,
                               callable_update_series_installments)
    update_value(new_value, diff_values)


def update_date_series(old_value, values,
                       callable_get_launch_series: Callable[[ObjectId], List[Launch]],
                       callable_persist_update_launch_date: Callable[[Launch], None]):
    key = next(filter(lambda k: 'date' in k, values.keys()), None)
    if key:
        launch_series = callable_get_launch_series(old_value.setting.launch_id)
        new_date = date.fromisoformat(values[key])
        old_date = date.fromisoformat(old_value.date)

        for launch_to_update in launch_series:
            launch_date = date.fromisoformat(launch_to_update.date)

            if new_date.year != old_date.year:
                launch_date = launch_date + relativedelta(years=(new_date.year - old_date.year))

            if new_date.month != old_date.month:
                launch_date = launch_date + relativedelta(months=(new_date.month - old_date.month))

            if new_date.day != old_date.day:
                last_date_month = launch_date.replace(day=calendar.monthrange(launch_date.year,
                                                                              launch_date.month)[1])
                dt = launch_date + relativedelta(days=(new_date.day - old_date.day))
                launch_date = last_date_month if dt > last_date_month else dt

            launch_to_update.date = str(launch_date)
            callable_persist_update_launch_date(launch_to_update)
        values.pop(key)


def update_installments_series(old_launch: Launch, new_launch: Launch, values,
                               callable_remove_launches: Callable[[Launch, int], None],
                               callable_persist_new_launches: Callable[[ObjectId, List[Launch]], None],
                               callable_update_series_installments: Callable[[Launch, float, int], None]):
    key = next(filter(lambda k: 'total_installments' in k, values.keys()), None)
    if key:
        new_amount_installments = values[key]
        old_amount_installments = old_launch.setting.total_installments
        new_value = get_total_values(values, old_launch, old_amount_installments, new_amount_installments)
        new_launch.value = new_value

        if new_amount_installments < old_amount_installments:
            callable_remove_launches(old_launch, new_amount_installments)

        elif new_amount_installments > old_amount_installments:
            old_launch.value = new_value
            old_launch.setting.total_installments = new_amount_installments
            launches = process_repeat(old_launch, start=old_amount_installments)
            del launches[0]  # Removes first launch to avoid duplicate on db
            callable_persist_new_launches(old_launch.category_id, launches)

        callable_update_series_installments(old_launch, new_value, new_amount_installments)
        values.pop(key)


def update_value(launch, values):
    value_key = next(filter(lambda k: 'value' in k, values.keys()), None)
    if value_key:
        if launch.setting.option_frequency in ['parcel']:
            values[value_key] = round(values[value_key] / launch.setting.total_installments, 2)


def get_total_values(values, old_launch, old_amount_installments, new_amount_installments):
    total_value_key = next(filter(lambda k: 'value' in k, values.keys()), None)

    if old_launch.setting.option_frequency == 'repeat':
        return values[total_value_key] if total_value_key else old_launch.value
    elif old_launch.setting.option_frequency == 'parcel':
        if total_value_key:
            total_value = round(values[total_value_key] / new_amount_installments, 2)
            values.pop(total_value_key)
        else:
            total_value = round((old_launch.value * old_amount_installments) / new_amount_installments, 2)
        return total_value


def process_repeat(launch, start=1):
    launches = [Launch(
        _id=ObjectId(),
        description=launch.description,
        value=launch.value,
        status=launch.status,
        category_id=launch.category_id,
        date=add_month(launch.date, current_parcel),
        setting=Setting(
            ObjectId(),
            launch.id,
            launch.setting.frequency,
            launch.setting.option_frequency,
            current_installment=current_parcel + 1,
            total_installments=launch.setting.total_installments
        )
    ) for current_parcel in range(start, launch.setting.total_installments)]
    launch.setting.launch_id = launch.id
    launches.insert(0, launch)
    return convert_to_dict(launches)


def process_parcel(launch):
    launch.value = round(launch.value / launch.setting.total_installments, 2)
    return process_repeat(launch)


def process_unique(launch):
    if launch.id == launch.setting.launch_id:
        del launch.date
    return convert_to_dict([launch])


def add_month(str_date, num_months):
    return str(date.fromisoformat(str_date) + relativedelta(months=num_months))
