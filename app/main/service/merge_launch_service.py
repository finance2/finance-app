from typing import List, Optional

from app.main.model.category import Category


def merge_launches(result, category) -> List[Category]:
    category_founded: Optional[Category] = next(filter(lambda c: c.title == category.title, result), None)
    if category_founded:
        for launch in category.launches:
            launch.category_id = category_founded.id
        category_founded.launches.extend(category.launches)
    else:
        result.append(category)
    return result


def remove_duplicates(category: Category):
    noduplicate_list = []
    while len(category.launches) > 0:
        launch = category.launches[0]
        duplicates = list(filter(lambda l: l.setting.launch_id == launch.setting.launch_id, category.launches))
        noduplicate_list.append(remove_launch_plannings(duplicates))
        remove_values_from_list(category.launches, duplicates)
    category.launches = noduplicate_list


def remove_values_from_list(launches: List, values):
    for value in values:
        launches.remove(value)


def remove_launch_plannings(duplicates):
    if len(duplicates) == 1:
        return duplicates[0]

    for value in duplicates:
        if not value.is_planning and value.date \
                or value.date:
            return value
