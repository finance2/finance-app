from bson import ObjectId

from app.main.model.launch import Launch, Setting
from app.main.error.launch_setting_not_informed import LaunchSettingNotInformed


def dict_to_launch(data):
    return Launch(
        _id=ObjectId(data.get('id') or data.get('_id')),
        description=data.get('description'),
        value=float(data.get('value')) if data.get('value') else 0.0,
        status=data.get('status'),
        category_id=ObjectId(data.get('category_id')),
        setting=_dict_to_launch_setting(data.get('setting')),
        date=data.get('date'),
    )

def dict_to_launch_update(data):
    return dict_to_launch(data.get('launch')), data.get('is_update_all')


def _dict_to_launch_setting(data):
    if data:
        return Setting(
            _id=ObjectId(data.get('id') or data.get('_id')),
            launch_id=ObjectId(data.get('launch_id')),
            frequency=data.get('frequency'),
            option_frequency=data.get('option_frequency') or 'unique',
            current_installment=data.get('current_installment'),
            total_installments=data.get('total_installments')
        )
    else:
        raise LaunchSettingNotInformed()

def launch_to_dict(launch: Launch):
    launch.setting = _setting_to_dict(launch.setting)
    return vars(launch)

def _setting_to_dict(setting: Setting):
    return vars(setting)