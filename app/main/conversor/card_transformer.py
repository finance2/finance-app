from bson import ObjectId

from app.main.model.card import Card
from app.main.service.dto.response.card.card_expensed_response import CardExpensedResponse
from app.main.service.dto.response.card.card_response import CardResponse


def from_create_card_request(request: dict) -> Card:
    return Card(
        _id=ObjectId(),
        title=request.get('title'),
        expensed=request.get('expensed'),
        owner=request.get('owner'),
        number=request.get('number'),
        colors=request.get('colors'),
        flag=request.get('flag')
    )


def from_update_card_request(request: dict) -> Card:
    card_id = ObjectId(request.get('id') or request.get('_id'))
    return Card(
        _id=card_id,
        title=request.get('title'),
        expensed=request.get('expensed'),
        owner=request.get('owner'),
        number=request.get('number'),
        colors=request.get('colors'),
        flag=request.get('flag')
    )


def from_dict(data: dict) -> Card:
    return Card(
        _id=ObjectId(data.get('id') or data.get('_id')),
        title=data.get('title'),
        expensed=data.get('expensed'),
        owner=data.get('owner'),
        number=data.get('number'),
        colors=data.get('colors'),
        flag=data.get('flag')
    )


def from_card_model(card: Card) -> CardResponse:
    return CardResponse(
        id=card.id,
        title=card.title,
        expensed=card.expensed,
        flag=card.flag,
        owner=card.owner,
        planning=0.0,
        colors=card.colors,
        number=card.number
    )


def to_card_expensed_response(data: dict) -> CardExpensedResponse:
    return CardExpensedResponse(
        card_id=data.get('_id'),
        expensed=data.get('expensed')
    )
