from bson import ObjectId

from app.main.model.category import Category


def convert_data_to_category(data):
    category_id = ObjectId(data.get('id') or data.get('_id'))

    return Category(
        _id=category_id,
        title=data.get('title'),
        color=data.get('color'),
        category_type=data.get('category_type'),
        planning=data.get('planning'),
        position=data.get('position'),
    )
