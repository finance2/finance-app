from bson import ObjectId

from app.main.model.planning import Planning
from app.main.conversor.launch_conversor import dict_to_launch


def convert_data_to_planning(data):
    planning_id = ObjectId(data.get('id') or data.get('_id'))
    launches = data.get('launches')

    launches = list(map(dict_to_launch, launches if isinstance(launches, list) else [launches])) \
        if data.get('launches') else []

    return Planning(
        _id=planning_id,
        title=data.get('title'),
        color=data.get('color'),
        category_type=data.get('category_type'),
        launches=launches,
        planning=data.get('planning'),
        position=data.get('position')
    )
