from datetime import datetime
from typing import List

from bson import ObjectId
from dateutil.relativedelta import relativedelta

from app.main.model.card_launch import CardLaunch, Setting as LaunchSetting
from app.main.service.dto.request.card_launch.add_card_launch_request import AddCardLaunchRequest, Setting
from app.main.service.dto.response.card_launch.card_launch_response import (
    CardLaunchResponse,
    Setting as CardLaunchSettingResponse
)


def to_add_card_launch_request(request: dict) -> AddCardLaunchRequest:
    return AddCardLaunchRequest(
        description=request.get('description'),
        value=request.get('value'),
        category=request.get('category'),
        date=request.get('date'),
        card_id=request.get('card_id'),
        setting=_to_setting(request.get('setting'))
    )


def _to_setting(data: dict) -> Setting:
    return Setting(
        frequency=data.get('frequency'),
        option_frequency=data.get('option_frequency'),
        current_installment=data.get('current_installment'),
        total_installments=data.get('total_installments')
    )


def from_add_card_launch_request(request: AddCardLaunchRequest) -> CardLaunch:
    launch_id = ObjectId()
    return CardLaunch(
        _id=launch_id,
        description=request.description,
        date=request.date,
        value=request.value,
        card_id=request.card_id,
        category=request.category,
        setting=_to_launch_setting(request.setting, str(launch_id))
    )


def _to_launch_setting(setting: Setting, launch_id: str) -> LaunchSetting:
    return LaunchSetting(
        _id=ObjectId(),
        launch_id=launch_id,
        frequency=setting.frequency,
        option_frequency=setting.option_frequency,
        total_installments=setting.total_installments,
        current_installment=setting.current_installment
    )


def generate_repeated_launches(request: AddCardLaunchRequest) -> List[CardLaunch]:
    card_launch = from_add_card_launch_request(request)

    launches = [
        CardLaunch(
            ObjectId(),
            date=_add_month(card_launch.date, parcel),
            value=card_launch.value,
            card_id=card_launch.card_id,
            category=card_launch.category,
            description=card_launch.description,
            setting=LaunchSetting(
                _id=ObjectId(),
                launch_id=str(card_launch.id),
                frequency=card_launch.setting.frequency,
                option_frequency=card_launch.setting.option_frequency,
                total_installments=card_launch.setting.total_installments,
                current_installment=parcel + 1
            )
        )
        for parcel in range(card_launch.setting.total_installments)
    ]
    launches[0] = card_launch

    return launches


def _add_month(date: str, plus_months: int) -> str:
    value = datetime.strptime(date, '%Y-%m-%d')
    return str(value.date() + relativedelta(months=plus_months))


def to_update_card_launch_request(data: dict) -> CardLaunch:
    return CardLaunch(
        ObjectId(data.get('id')),
        date=None,
        card_id=None,
        description=data.get('description'),
        category=data.get('category'),
        value=data.get('value'),
        setting=LaunchSetting(
            _id=None,
            frequency=None,
            option_frequency=None,
            total_installments=None,
            current_installment=None,
            launch_id=data.get('setting').get('launch_id')
        )
    )


def to_card_launch_response(card_launch: CardLaunch) -> CardLaunchResponse:
    return CardLaunchResponse(
        id=str(card_launch.id),
        description=card_launch.description,
        value=card_launch.value,
        category=card_launch.category,
        date=card_launch.date,
        card_id=card_launch.card_id,
        setting=CardLaunchSettingResponse(
            launch_id=card_launch.setting.launch_id,
            frequency=card_launch.setting.frequency,
            option_frequency=card_launch.setting.option_frequency,
            current_installment=card_launch.setting.current_installment,
            total_installments=card_launch.setting.total_installments
        )
    )


def to_card_launch_dict(card_launch: CardLaunch):
    card_launch.setting = vars(card_launch.setting)
    return vars(card_launch)


def from_dict(data: dict) -> CardLaunch:
    return CardLaunch(
        ObjectId(data.get('_id')),
        description=data.get('description'),
        value=data.get('value'),
        category=data.get('category'),
        date=data.get('date'),
        card_id=data.get('card_id'),
        setting=_from_dict(data.get('setting'))
    )


def _from_dict(data: dict) -> LaunchSetting:
    return LaunchSetting(
        ObjectId(data.get('id')),
        launch_id=data.get('launch_id'),
        frequency=data.get('frequency'),
        option_frequency=data.get('option_frequency'),
        current_installment=data.get('current_installment'),
        total_installments=data.get('total_installments')
    )
