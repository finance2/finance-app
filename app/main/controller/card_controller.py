from typing import List

from flask import request
from flask_restplus import Resource, marshal

import app.main.service.card_service as service
from app.main.controller import card_ns as api
from app.main.controller.dto.request.card import (
    CreateCardRequest,
    UpdateCardRequest
)
from app.main.controller.dto.request.launch_card import (
    AddCardLaunchRequest,
    UpdateCardLaunchRequest
)
from app.main.controller.dto.response.launch_card import (
    LaunchCardResponse
)
from app.main.controller.dto.response.card import (
    CardResponse as CardResponseModel,
    CreateCardResponse as CreateCardResponseModel
)
from app.main.service.dto.response.card.card_response import CardResponse
from app.main.service.dto.response.card_launch.card_launch_response import CardLaunchResponse


@api.route('/')
class Card(Resource):

    @api.doc('List of cards')
    @api.marshal_list_with(CardResponseModel.model)
    def get(self) -> List[CardResponse]:
        month_year = request.args.get('monthYear')
        return service.list_cards(month_year)

    @api.doc('Register a new card')
    @api.expect(CreateCardRequest.model, validate=True)
    @api.response(201, 'Card successfully added', model=CreateCardResponseModel.model)
    def post(self):
        """Register a new card"""
        card_id = service.create_card(marshal(request.json, CreateCardRequest.model))
        response = {'status': 'success', 'message': 'Card successfully added', 'id': card_id}
        return response, 201

    @api.doc('Update an existent card')
    @api.expect(UpdateCardRequest.model, validate=True)
    @api.response(204, 'Card sucessfully updated')
    def put(self):
        """Update an existent card"""
        service.update_card(marshal(request.json, UpdateCardRequest.model))
        return None, 204


@api.route('/<string:card_id>')
@api.doc(params={'card_id': 'Id card'})
class CardId(Resource):
    @api.doc('Card Detail')
    @api.marshal_with(CardResponseModel.model)
    def get(self, card_id) -> CardResponse:
        return service.find_card_by_id(card_id)

    @api.doc('Remove card')
    def delete(self, card_id):
        service.remove_card(card_id)

        response = {
            'status': 'success',
            'message': 'Card removed successfully'
        }
        return response, 204


@api.route('/launches')
class LaunchCard(Resource):
    @api.doc('Adds launch on card')
    @api.expect(AddCardLaunchRequest.model, validate=True)
    def post(self):
        """Adds a launch on a card"""
        data = marshal(request.json, AddCardLaunchRequest.model)
        launch_id = service.add_card_launch(data)

        response = {
            'id': launch_id,
            'status': 'success',
            'message': 'Launch successfully added'
        }
        return response, 201

    @api.doc('Update launch card')
    @api.expect(UpdateCardLaunchRequest.model)
    def put(self):
        """Updates a launch card"""
        data = marshal(request.json, UpdateCardLaunchRequest.model)
        service.update_launch_card(data)
        return None, 204

    @api.doc('List launch cards by month year')
    @api.marshal_list_with(LaunchCardResponse.model)
    def get(self) -> List[CardLaunchResponse]:
        month_year = request.args.get('monthYear')
        card_id = request.args.get('cardId')
        return service.find_card_launches_by_month(month_year, card_id)


@api.route('/launches/<string:launch_id>')
class LaunchCardId(Resource):
    @api.doc('Remove series launch with id')
    def delete(self, launch_id):
        service.remove_launch_card(launch_id)
        return None, 204
