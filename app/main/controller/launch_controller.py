from typing import List
from flask import request
from flask_restplus import Resource, fields, marshal

from app.main.dto.launch import api
from app.main.dto.launch.launch_dto import LaunchDto
from app.main.dto.launch.update_launch_dto import UpdateLaunchDto
from app.main.model.launch import Launch
import app.main.service.launch_service as launch_service


@api.route('/')
class AddLaunch(Resource):
    @api.doc(description='Save a new launch on category')
    @api.expect(LaunchDto.launch, validate=True)
    @api.response(201, 'Launch successfully added', model=api.model('launchId', {'id': fields.String()}))
    def post(self):
        """Save a new launch on category"""
        return launch_service.generate_launches(marshal(request.json, LaunchDto.launch))

    @api.doc(description='Update a launch on category')
    @api.expect(UpdateLaunchDto.updateLaunch, validate=True)
    @api.response(200, 'Launch successfully updated')
    def put(self):
        """Update a launch"""
        return launch_service.update_launch(marshal(request.json, UpdateLaunchDto.updateLaunch))

    @api.doc(description='List launches by month of year')
    @api.marshal_list_with(LaunchDto.launch)
    def get(self) -> List[Launch]:
        month_year = request.args.get('monthYear')
        return launch_service.launches_by_monty_year(month_year)


@api.route('/<string:launch_id>')
@api.doc(params={'launch_id': 'Id launch to remove'})
class RemoveLaunch(Resource):
    @api.doc(description='Delete a launch by id')
    @api.expect(fields.String, validate=True)
    @api.response(200, 'Launch successfully removed')
    def delete(self, launch_id):
        """Remove a launch """
        return launch_service.remove_launch(launch_id)
    
    @api.marshal_with(LaunchDto.launch)
    def get(self, launch_id):
        """Find launch by id"""
        return launch_service.find_launch_by_id(launch_id)
