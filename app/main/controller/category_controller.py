from typing import List
from flask import request
from flask_restplus import Resource, fields, marshal

import app.main.service.category_service as category_service
from app.main.util.dto import CategorizedLaunches, CategoryDto
from app.main.service.dto.response.category.categorized_launches_response import CategorizedLaunchesResponse


api = CategoryDto.api
_category = CategoryDto.category


@api.route('/')
class Category(Resource):
    @api.doc('Save a new category')
    @api.response(201, 'Category successfully registered', model=api.model('categoryId', {'id': fields.String()}))
    @api.expect(_category, validate=True)
    def post(self):
        """ Save a new category """
        return category_service.create_category(marshal(request.json, _category))

    @api.doc('Update a category')
    @api.response(201, 'Category successfully updated')
    @api.expect(_category, validate=True)
    def put(self):
        """Update a category"""
        return category_service.update_category(marshal(request.json, _category))

@api.route('/<string:month_year>')
@api.doc(params={'month_year': 'month-year of launches'})
class CategoryLaunchesMonth(Resource):
    @api.doc('List category with launches')
    @api.marshal_list_with(CategorizedLaunches.categorized_launches)
    def get(self, month_year) -> List[CategorizedLaunchesResponse]:
        """ List launches categorized """
        show_empty_categories = request.args.get('showEmptyCategories', default=False, type=lambda v: v.lower() == 'true')
        return category_service.list_from_month_year(month_year, show_empty_categories)
