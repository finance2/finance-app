import enum

from flask_restplus import fields

from app.main.controller import card_ns as api


class FlagType(enum.Enum):
    mastercard = 'mastercard'
    visa = 'visa'


class CardResponse:
    model = api.model(name='Card', model={
        'id': fields.String(description='card identified'),
        'title': fields.String(description='title of card'),
        'colors': fields.String(description='colors of card', default='#0f92ba,#52aa0c'),
        'expensed': fields.Float(description='value expensed on card', default=0.0),
        'planning': fields.Float(description='value planning expense', default=0.0),
        'owner': fields.String(description='name owner card'),
        'number': fields.String(description='last 4 numbers card', default='xxxx'),
        'flag': fields.String(description='flag of card', enum=[flag.name for flag in FlagType])
    })


class CreateCardResponse:
    model = api.model(name='Create Card Response', model={
        'id': fields.String(description='Card Id')
    })
