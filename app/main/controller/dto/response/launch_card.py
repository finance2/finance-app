from flask_restplus import fields

from app.main.controller import card_ns as api


class NullableString(fields.String):
    __schema_type__ = ['string', 'null']
    __schema_example__ = 'nullable string'


class LaunchCardResponse:
    model = api.model(name='Launch Card Response', model={
        'id': fields.String(description='launch id'),
        'description': fields.String(description='description launch'),
        'value': fields.Float(description='value of launch', default=0.0),
        'category': fields.String(description='category identified where launch is added'),
        'date': NullableString(description='date of launch'),
        'card_id': fields.String(description='card of launch'),
        'setting': fields.Nested(api.model('Setting', {
            'launch_id': fields.String(description='main launch id'),
            'frequency': fields.String(description='frequency of launch'),
            'option_frequency': fields.String(description='option of frequency'),
            'current_installment': fields.Integer(description='current installment of launch'),
            'total_installments': fields.Integer(description='total of installment launch')
        }))
    })
