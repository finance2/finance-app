import enum

from flask_restplus import fields

from app.main.controller import card_ns as api


class FlagType(enum.Enum):
    mastercard = 'mastercard'
    visa = 'visa'


class CreateCardRequest:
    model = api.model(name='Create Card Request', model={
        'title': fields.String(description='title of card', required=True),
        'expensed': fields.Float(description='value expensed on card', default=0.0),
        'owner': fields.String(description='name owner card', required=True),
        'number': fields.String(description='last 4 numbers card', default='xxxx'),
        'colors': fields.String(description='colors of card', default='#0f92ba,#52aa0c'),
        'flag': fields.String(description='flag of card', enum=[flag.name for flag in FlagType])
    })


class UpdateCardRequest:
    model = api.model(name='Update Card Request', model={
        'id': fields.String(description='Card Id'),
        'title': fields.String(description='title of card', required=True),
        'expensed': fields.Float(description='value expensed on card', default=0.0),
        'owner': fields.String(description='name owner card', required=True),
        'number': fields.String(description='last 4 numbers card', required=True),
        'colors': fields.String(description='colors of card', required=True),
        'flag': fields.String(description='flag of card', enum=[flag.name for flag in FlagType], required=True)
    })
