from flask_restplus import fields

from app.main.controller import card_ns as api


class NullableString(fields.String):
    __schema_type__ = ['string', 'null']
    __schema_example__ = 'nullable string'


class AddCardLaunchRequest:
    model = api.model(name='Add Card Launch', model={
        'description': fields.String(description='description launch', required=True),
        'value': fields.Float(description='value of launch', default=0.0),
        'category': fields.String(description='category identified where launch is added'),
        'date': NullableString(description='date of launch', required=True),
        'card_id': fields.String(description='card of launch', required=True),
        'setting': fields.Nested(api.model('Setting', {
            'frequency': fields.String(description='frequency of launch', default='Mensal'),
            'option_frequency': fields.String(description='option of frequency', default='unique'),
            'current_installment': fields.Integer(description='current installment of launch', default=0),
            'total_installments': fields.Integer(description='total of installment launch', default=1)
        }))
    })


class UpdateCardLaunchRequest:
    model = api.model(name='Update Card Launch', model={
        'id': fields.String(description='launch id', required=True),
        'description': fields.String(description='description launch', required=True),
        'value': fields.Float(description='value of launch', default=0.0),
        'category': fields.String(description='category identified where launch is added'),
        'setting': fields.Nested(api.model('Update Card Launch Setting', {
            'launch_id': fields.String(description='main launch id', required=True)
        }))
    })
