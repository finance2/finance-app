from flask import request
from flask_restplus import Resource, fields

from app.main.dto.launch.launch_dto import LaunchDto
from app.main.service import planning_service
from app.main.util.dto import PlanningDto

api = PlanningDto.api


@api.route('/')
class Planning(Resource):
    @api.doc('Return planning of user')
    @api.marshal_list_with(PlanningDto.planning)
    def get(self):
        """Return planning of user"""
        month_year = request.args.get('monthYear')
        return planning_service.list_planning(month_year)

    @api.doc('Save a new planning category')
    @api.response(201, 'Planning category successfully created', model=api.model('categoryId', {'id': fields.String()}))
    @api.expect(PlanningDto.planning, validate=True)
    def post(self):
        """Create a new planning"""
        return planning_service.save_planning(request.json)

    @api.doc('Update a planning category')
    @api.response(201, 'Planning category successfully updated')
    @api.expect(PlanningDto.planning, validate=True)
    def put(self):
        """Update a planning"""
        return planning_service.update_planning(request.json)


@api.route('/launch/')
class PlanningLaunch(Resource):
    @api.doc(description='Save a new launch on planning category')
    @api.expect(LaunchDto.launch, validate=True)
    @api.response(201, 'Launch successfully registered on planning', model=api.model('launchId',
                                                                                     {'id': fields.String()}))
    def post(self):
        """Save a new launch on planning category"""
        return planning_service.add_launch(request.json)

    @api.doc(description='Update a launch on planning category')
    @api.expect(LaunchDto.launch, validate=True)
    @api.response(201, 'Launch successfully updated')
    def put(self):
        """Update a launch on planning category"""
        return planning_service.update_launch(request.json)
