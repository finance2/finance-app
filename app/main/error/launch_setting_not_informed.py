class LaunchSettingNotInformed(ValueError):
    def __init__(self, message='Launch setting not informed!') -> None:
        self.message = message
        super().__init__(message=self.message)