from flask_restplus import Namespace

api = Namespace('Launches', description='Data to update one or many launches')
