from flask_restplus import fields

from app.main.dto.launch import api
from app.main.model.status import Status


class NullableString(fields.String):
    __schema_type__ = ['string', 'null']
    __schema_example__ = 'nullable string'


class LaunchDto:
    launch = api.model('Launch', {
        'id': fields.String(description='launch identified'),
        'description': fields.String(description='description of launch', required=True),
        'value': fields.Float(description='value of launch', default=0.0),
        'status': fields.String(description='status of launch', enum=[s.name for s in Status]),
        'category_id': fields.String(description='category identified where launch is added'),
        'date': NullableString(description='date of launch'),
        'setting': fields.Nested(api.model('Setting', {
            '_id': fields.String(description='setting identified'),
            'launch_id': fields.String(description='launch identified'),
            'frequency': fields.String(description='frequency of launch', default='Mensal'),
            'option_frequency': fields.String(description='option of frequency', default='unique'),
            'current_installment': fields.Integer(description='current installment of launch', default=0),
            'total_installments': fields.Integer(description='total of installment launch', default=1)
        }))
    })
