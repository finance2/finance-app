from flask_restplus import fields

from app.main.dto.launch.launch_dto import LaunchDto
from app.main.dto.launch import api


class UpdateLaunchDto:
    updateLaunch = api.model('Update Launch', {
        'is_update_all': fields.Boolean(description='Check if update all series launch', attribute='isUpdateAll', default=False),
        'launch': fields.Nested(api.model('Launch', LaunchDto.launch))
    })
