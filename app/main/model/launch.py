class Setting:
    _id: None

    def __init__(self, _id, launch_id, frequency: str,
                 option_frequency: str, current_installment: int, total_installments: int):
        self._id = _id
        self.launch_id = launch_id
        self.frequency = frequency
        self.option_frequency = option_frequency
        self.current_installment = current_installment
        self.total_installments = total_installments

    @property
    def id(self):
        return self._id


class Launch:
    _id: None

    def __init__(self, _id, description: str, value: float, status: str, category_id, setting: Setting, date=None):
        self._id = _id
        self.description = description
        self.value = value
        self.status = status
        self.category_id = category_id
        self.setting = setting
        self.date = date

    @property
    def id(self):
        return self._id
