class Card:
    _id = None

    def __init__(self, _id, title: str, expensed: float, owner: str, number: str, flag: str, colors: str):
        self._id = _id
        self.title = title
        self.expensed = expensed
        self.owner = owner
        self.number = number
        self.flag = flag
        self.colors = colors

    @property
    def id(self):
        return self._id
