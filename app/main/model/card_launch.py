class Setting:
    _id: None

    def __init__(self, _id, launch_id, frequency: str,
                 option_frequency: str, current_installment: int, total_installments: int):
        self._id = _id
        self.launch_id = launch_id
        self.frequency = frequency
        self.option_frequency = option_frequency
        self.current_installment = current_installment
        self.total_installments = total_installments

    @property
    def id(self):
        return self._id


class CardLaunch:
    _id = None

    def __init__(self,
                 _id,
                 description: str,
                 value: float,
                 category: str,
                 date: str,
                 card_id: str,
                 setting: Setting):
        self._id = _id
        self.description = description
        self.value = value
        self.category = category
        self.date = date
        self.card_id = card_id
        self.setting = setting

    @property
    def id(self):
        return self._id
