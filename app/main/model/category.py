class Category:
    _id = None

    def __init__(self, _id, title, color, category_type, planning, position):
        self._id = _id
        self.title = title
        self.color = color
        self.category_type = category_type
        self.planning = planning
        self.position = position

    def __str__(self):
        return self._id

    @property
    def id(self):
        return self._id
