import enum


class Status(enum.Enum):
    paid: 'paid'
    scheduled: 'scheduled'
    late: 'late'
