import logging
from typing import List

from bson import ObjectId

from app.main import mongo
from app.main.conversor import (
    card_launch_transformer as transformer,
    card_transformer
)
from app.main.model.card_launch import CardLaunch

log = logging.getLogger(__name__)


def save_card_launch(card_launches: List[CardLaunch]):
    log.info(f'Saving card launch: {card_launches}')
    launches = list(map(transformer.to_card_launch_dict, card_launches))

    mongo.db.card_launch.insert_many(launches)


def update_card_launch(card_launch: CardLaunch):
    log.info(f'Updating card launch {card_launch}')
    mongo.db.card_launch.update_many(
        {'setting.launch_id': card_launch.setting.launch_id},
        {'$set': {
            'description': card_launch.description,
            'value': card_launch.value,
            'category': card_launch.category,
        }}
    )


def remove_card_launch(launch_id: str):
    log.info(f'Finding main launch id from launch {launch_id}')
    data = mongo.db.card_launch.find_one({'_id': ObjectId(launch_id)}, {'setting.launch_id': 1})

    if data is not None:
        main_launch_id = data.get('setting').get('launch_id')
        log.info(f'Removing card launches with main launch id {main_launch_id}')

        mongo.db.card_launch.delete_many({'setting.launch_id': main_launch_id})


def remove_card_launches_by_card(card_id: str):
    log.info(f'Removing launches from card {card_id}')
    mongo.db.card_launch.delete_many({'card_id': card_id})


def find_cards_launch_by_month(month_year: str, card_id: str) -> List[CardLaunch]:
    log.info(f'Searching card launches by month {month_year}')
    query = {'$or': [
        {'setting.option_frequency': 'unique'},
        {'date': {
            '$gte': f'{month_year}-01',
            '$lte': f'{month_year}-31'
        }}]
    }

    if card_id is not None:
        query['card_id'] = card_id

    values = mongo.db.card_launch.find(query)

    log.info(f'card launches found {values}')
    return list(map(transformer.from_dict, values))


def find_total_expensed_cards_by(month_year: str):
    log.info(f'Calculating total expensed cards from month {month_year}')
    values = mongo.db.card_launch.aggregate([
        {'$match': {'$or': [
            {'setting.option_frequency': 'unique'},
            {'date': {
                '$gte': f'{month_year}-01',
                '$lte': f'{month_year}-31'
            }}]
        }},
        {'$group': {
            '_id': '$card_id',
            'expensed': {
                '$sum': '$value'
            }
        }}
    ])

    cards = list(map(card_transformer.to_card_expensed_response, values))
    log.info(f'Total cards {cards}')
    return cards
