import logging
from typing import List

from bson import ObjectId

import app.main.conversor.card_transformer as transformer
from app.main import mongo
from app.main.error.card_not_found_error import CardNotFoundError
from app.main.model.card import Card

log = logging.getLogger(__name__)


def save_card(card: Card):
    log.info(f'Saving card: ${card}')
    data = vars(card)
    mongo.db.card.insert_one(data)

    log.info(f'New card added: {data}')


def update_card(card: Card):
    mongo.db.card.update_one(
        filter={'_id': card.id},
        update={'$set': {
            'title': card.title,
            'owner': card.owner,
            'number': card.number,
            'flag': card.flag,
            'colors': card.colors
        }}
    )
    log.info(f'Card {card} updated')


def remove_card(card_id: str):
    log.info(f'Removing card with id {card_id}')
    mongo.db.card.delete_one({'_id': ObjectId(card_id)})


def card_by_id(card_id: str) -> Card:
    data = mongo.db.card.find_one({'_id': ObjectId(card_id)})

    if data is None:
        log.warning(f'Card with id {card_id} does not exist')
        raise CardNotFoundError(f'Card with id {card_id} does not exist')

    log.info(f'Card with id {card_id} found: {data}')
    return transformer.from_dict(data)


def list_cards() -> List[Card]:
    datas = mongo.db.card.find()

    log.info(f'Cards founded {datas}')
    cards = list(map(transformer.from_dict, datas))

    return cards
