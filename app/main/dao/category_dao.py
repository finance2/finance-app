import logging
from typing import List

from app.main import mongo
from app.main.model.category import Category
from app.main.conversor import category_conversor

log = logging.getLogger(__name__)

def update_category(category: Category):
    mongo.db.category.update_one({'_id': category.id},
                                 {'$set': {
                                     'title': category.title,
                                     'color': category.color,
                                     'category_type': category.category_type,
                                     'planning': category.planning,
                                     'position': category.position
                                 }})
    log.info('Category {} updated to {}'.format(category.id, vars(category)))


def insert_category(category: Category):
    mongo.db.category.insert_one(vars(category))
    log.info('New category added: {}'.format(vars(category)))


def find_by_list_ids(categories_id: List[str], show_empty_categories = False):
    datas = mongo.db.category.find({} if show_empty_categories else {'_id': {'$in': categories_id}})
    return list(map(category_conversor.convert_data_to_category, datas))
