import logging
from bson import ObjectId
from functools import reduce
from typing import List, Optional

from app.main import mongo
from app.main.conversor import launch_conversor
from app.main.conversor.category_conversor import convert_data_to_category
from app.main.error.launch_not_found_error import LaunchNotFoundError
from app.main.model.launch import Launch
from app.main.service.merge_launch_service import merge_launches

log = logging.getLogger(__name__)


def save_launch(category_id, launches: List[Launch]):
    """
        Save a new launch to category

            Parameters:
                category_id (ObjectId): Category ID
                launches ([Launch]): List of launches to save
    """
    launches = list(map(launch_conversor.launch_to_dict, launches))
    mongo.db.launch.insert_many(launches)
    log.info('Launches {} added on category {}'.format(launches, category_id))


def update_launch(launch: Launch):
    mongo.db.launch.update_one(
        {'_id': launch.id},
        {'$set': {
            'description': launch.description,
            'value': launch.value,
            'date': launch.date,
            'status': launch.status
        }}
    )
    log.info('Updated launch {} from id {}'.format(vars(launch), launch.id))


def update_all(launch: Launch):
    mongo.db.launch.update_many(
        {'setting.launch_id': launch.setting.launch_id},
        {'$set': {
            'description': launch.description,
            'value': launch.value
        }}
    )


def persist_changes(launch: Launch, values):
    if values:
        mongo.db.category.update_one(
            {'_id': launch.category_id},
            {'$set': values},
            array_filters=[
                {"launch.setting.launch_id": launch.setting.launch_id}
            ]
        )
        log.info('Persisted changes launch {} from id {}'.format(values, launch.setting.launch_id))


def update_amount_installments_and_value(launch: Launch, new_value: float, new_amount_installments: int):
    mongo.db.planning.update_one(
        {'_id': launch.category_id},
        {'$set': {
            'launches.$[launch].setting.total_installments': new_amount_installments,
            'launches.$[launch].value': new_value
        }},
        array_filters=[{"launch.setting.launch_id": launch.setting.launch_id}]
    )
    log.info('Updated amount installment to {} from launch id {}'.format(new_amount_installments,
                                                                         launch.setting.launch_id))


def persist_update_launch_date(launch):
    mongo.db.category.update_one(
        {'_id': launch.category_id},
        {'$set': {'launches.$[launch].date': launch.date}},
        array_filters=[{"launch._id": launch.id}]
    )
    log.info('Launch date updated to {} from id {}'.format(launch.date, launch.id))

def get_launches_by_month_year(month_year) -> List[Launch]:
    values = mongo.db.launch.find({'$or': [
        {'date': None},
        {'date': month_year}
    ]})
    launches = list(map(launch_conversor.dict_to_launch, values))
    return _remove_duplicates(launches)


def _remove_duplicates(launches: List[Launch]) -> List[Launch]:
    """
    Filter launches type 'unique' with date definition
    and remove main launch started
    """
    launches_index = [
        index
        for index, launch in enumerate(launches)
        if launch.setting.option_frequency == 'unique'
        and launch.date is None

        # Find out unique launches frequency with date
        for launch_b in launches
        if launch_b.date is not None
        and launch_b.setting.launch_id == launch.id
    ]

    for index in launches_index:
        launches[index] = None

    return [launch for launch in launches if launch is not None]

def find_by_id(launch_id) -> Launch:
    launch = mongo.db.launch.find_one({'_id': ObjectId(launch_id)})

    if launch == None:
        log.warning(f'Launch with id: {launch_id} does not exist')
        raise LaunchNotFoundError(f'Launch with id: {launch_id} does not exist')

    return launch_conversor.dict_to_launch(launch)


def get_launches_series(launch_id) -> List[Launch]:
    values = mongo.db.category.aggregate([
        {'$project': {'title': 1, 'launches': 1}},
        {'$unwind': {
            'path': '$launches',
            'preserveNullAndEmptyArrays': True
        }},
        {'$match': {'launches.setting.launch_id': launch_id}}
    ])

    launches = list(map(convert_data_to_category, values))
    if len(launches) > 0:
        category = reduce(merge_launches, launches, [])[0]
        return category.launches
    else:
        return []


def check_category_is_not_exist(category_id) -> bool:
    cursor = mongo.db.category.find_one({'_id': category_id}, {'projection': {'_id': 1}})
    return cursor is None


def remove_launch_by_id(launch: Launch):
    mongo.db.launch.delete_one({'_id': launch.id})
    log.info(f'Launch with id ${launch.id} removed')


def remove_launches_by_index(launch: Launch, index_start_remove: int):
    mongo.db.category.update_one(
        {'_id': launch.category_id},
        {'$pull': {'launches': {
            'setting.launch_id': launch.setting.launch_id,
            'setting.current_installment': {'$gt': index_start_remove}
        }}}
    )
    log.info('Removed launches from category id {} and start index {}'.format(launch.category_id, index_start_remove))


def remove_launch_series(launch: Launch):
    mongo.db.launch.delete_many({'setting.launch_id': launch.setting.launch_id})
    log.info(f'Serie launch with id ${launch.id} removed')
