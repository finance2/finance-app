import logging
from functools import reduce

from bson import ObjectId

from app.main import mongo
from app.main.conversor.planning_conversor import convert_data_to_planning
from app.main.error.launch_not_found_error import LaunchNotFoundError
from app.main.error.planning_not_found_error import PlanningNotFoundError
from app.main.model.launch import Launch
from app.main.model.planning import Planning
from app.main.service.merge_launch_service import merge_launches
from app.main.util import dict_converter

log = logging.getLogger(__name__)


def create_planning(planning: Planning):
    mongo.db.planning.insert_one(vars(planning))


def update_planning_launch(launch: Launch):
    data = dict_converter.convert_to_dict(launch)
    mongo.db.planning.update_many(
        {'_id': launch.category_id},
        {'$set': {'launches.$[launch]': data}},
        array_filters=[
            {'launch._id': launch.setting.launch_id}
        ]
    )


def update_planning(planning: Planning):
    planning_id = planning.id
    delattr(planning, '_id')
    delattr(planning, 'launches')
    mongo.db.planning.update_one({'_id': planning_id},
                                 {'$set': vars(planning)})
    log.info('Persisted planning updated: {}'.format(vars(planning)))


def get_plannings_by(month_year):
    aggregations_groups = [{
        '$project': {'_id': 1, 'launches': 0}
    }]
    aggregations_value = [
        {'$project': {'title': 1, 'launches': 1}},
        {'$unwind': {
            'path': '$launches',
            'preserveNullAndEmptyArrays': True
        }},
        {'$match': {
            '$or': [
                {'launches.date': {'$gte': f'{month_year}-01', '$lte': f'{month_year}-31'}},
                {'launches.date': None}
            ]
        }}
    ]

    groups_planning = mongo.db.planning.aggregate(aggregations_groups)
    values = mongo.db.planning.aggregate(aggregations_value)

    groups_planning = list(map(convert_data_to_planning, groups_planning))
    values = list(map(convert_data_to_planning, values))
    return reduce(merge_launches, groups_planning + values, [])


def get_planning_by_id(planning_id: ObjectId) -> Planning:
    log.info('Loading planning by id: {}'.format(planning_id))
    data = mongo.db.planning.find_one({'_id': planning_id}, {'projection': {'launches': 0}})

    if data is None:
        message = f'Planning with id {planning_id} does not exist'
        log.warning(message)
        raise PlanningNotFoundError(message)
    return convert_data_to_planning(data)


def persist_launches(category_id, launches):
    mongo.db.planning.update_one(
        {'_id': category_id},
        {'$push': {'launches': {'$each': launches}}}
    )
    log.info('Persisted launches: {}'.format(launches))


def persist_update_launch_date(launch):
    mongo.db.planning.update_one(
        {'_id': launch.category_id},
        {'$set': {'launches.$[launch].date': launch.date}},
        array_filters=[{"launch._id": launch.id}]
    )


def persist_changes(launch, values):
    if values:
        mongo.db.planning.update_one(
            {'_id': launch.category_id},
            {'$set': values},
            array_filters=[
                {"launch.setting.launch_id": launch.setting.launch_id}
            ]
        )


def get_launches_series(launch):
    data = mongo.db.planning.aggregate([
        {'$project': {'title': 1, 'launches': 1}},
        {'$unwind': {
            'path': '$launches',
            'preserveNullAndEmptyArrays': True
        }},
        {'$match': {'launches.setting.launch_id': launch.setting.launch_id}}
    ])

    launches = list(map(convert_data_to_planning, data))
    if len(launches) > 0:
        planning = reduce(merge_launches, launches, [])[0]
        return planning.launches
    else:
        return []


def remove_launches_by_index(launch: Launch, index_start_remove: int):
    mongo.db.planning.update_one(
        {'_id': launch.category_id},
        {'$pull': {'launches': {
            'setting.launch_id': launch.setting.launch_id,
            'setting.current_installment': {'$gt': index_start_remove}
        }}}
    )
    log.info('Launch id removed {} on start index: {}'.format(launch.setting.launch_id, index_start_remove))


def update_amount_installments_and_value(launch: Launch, new_value: float, new_amount_installments: int):
    mongo.db.planning.update_one(
        {'_id': launch.category_id},
        {'$set': {
            'launches.$[launch].setting.total_installments': new_amount_installments,
            'launches.$[launch].value': new_value
        }},
        array_filters=[{"launch.setting.launch_id": launch.setting.launch_id}]
    )
    log.info('Updated amount installments to {} from launch id {}'.format(new_amount_installments,
                                                                          launch.setting.launch_id))


def get_launch(launch_id):
    cursor = mongo.db.planning.aggregate([
        {'$unwind': {'path': '$launches', 'preserveNullAndEmptyArrays': True}},
        {'$match': {'launches._id': launch_id}},
        {'$project': {'launches': 1}}
    ])
    planning = convert_data_to_planning(cursor.next())
    if len(planning.launches) == 0:
        raise LaunchNotFoundError(f'Launch with id: {launch_id} does not exist')

    return planning.launches[0]


def delete_planning_by_title(title):
    log.info('Start remove planning with title {}'.format(title))
    return mongo.db.planning.delete_many({'title': title})
