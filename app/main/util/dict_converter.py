import json

from bson import ObjectId


def convert_id(json_value):
    if not isinstance(json_value, list):  # convert values to list
        json_value = [json_value]

    for data in json_value:
        for value in filter(lambda v: type(v) is dict, data.values()):
            convert_id([value])

        for key in filter(lambda k: 'id' in k, data.keys()):
            value = data[key]
            data[key] = ObjectId(value)


def convert_to_dict(values):
    data = json.dumps(values, default=lambda l: vars(l) if hasattr(l, '__dict__') else str(l))
    result = json.loads(data)
    convert_id(result)

    return result
