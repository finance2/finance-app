import enum

from flask_restplus import Namespace, fields

from app.main.dto.launch.launch_dto import LaunchDto


class NullableString(fields.String):
    __schema_type__ = ['string', 'null']
    __schema_example__ = 'nullable string'


class CategoryType(enum.Enum):
    income = 'income'
    expense = 'expense'


class CategoryDto:
    api = Namespace('Categories', description='category of launch')
    category = api.model('Category', {
        'id': fields.String(description='category identified'),
        'title': fields.String(required=True, description='title category'),
        'color': fields.String(description='color to category'),
        'category_type': fields.String(description='type of category', default='income',
                          attribute='type', enum=[v.name for v in CategoryType]),
        'planning': fields.Float(description='total planning of category', default=0),
        'monthYear': NullableString(description='month and year of category', attribute='month_year'),
        'position': fields.Integer
    })


class CategorizedLaunches:
    api = Namespace('Categories', description='category with yours launches')
    categorized_launches = api.model('Category', {
        'id': fields.String(description='category identified'),
        'title': fields.String(required=True, description='title category'),
        'color': fields.String(description='color to category'),
        'type': fields.String(description='type of category', default='income',
                              enum=[v.name for v in CategoryType]),
        'planning': fields.Float(description='total planning of category', default=0),
        'position': fields.Integer,
        'launches': fields.List(fields.Nested(LaunchDto.launch))
    })


class PlanningDto:
    api = Namespace('Planning', description='planning of user')
    planning = api.model('Planning', {
        'id': fields.String(description='category identified'),
        'title': fields.String(required=True, description='title category'),
        'color': fields.String(description='color to category'),
        'type': fields.String(description='type of category', default='income',
                              attribute='category_type', enum=[v.name for v in CategoryType]),
        'planning': fields.Float(description='total planning of category', default=0),
        'position': fields.Integer,
        'launches': fields.List(fields.Nested(LaunchDto.launch))
    })
