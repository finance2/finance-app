import logging
from deepdiff import DeepDiff
from app.main.error.diff_values_error import DiffValuesError

log = logging.getLogger(__name__)

def get_diff_values_dict(old, new, root_replace=''):
    diff = DeepDiff(old, new)
    diff = diff.to_dict()
    diff = diff.get('values_changed')
    if diff is None:
        log.warning(f'Some problem on get changed values: values_changed key does not exist')
        raise DiffValuesError('values_changed key does not exist')
    return {k.replace('root.', root_replace): v.get('new_value') for k, v in diff.items()}
