from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_pymongo import PyMongo

from app.main.config import config_by_name

flask_bcrypt = Bcrypt()
mongo = PyMongo()


def create_app(config_name):
    app = Flask(__name__)
    CORS(app, resources={r"*": {"origins": "*"}})
    app.config.from_object(config_by_name[config_name])
    mongo.init_app(app)
    flask_bcrypt.init_app(app)

    return app
