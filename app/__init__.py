from flask import Blueprint
from flask_restplus import Api

from app.main.controller.category_controller import api as category_ns
from app.main.controller.launch_controller import api as launch_ns
from app.main.controller.planning_controller import api as planning_ns
from app.main.controller import (
    card_ns,
    card_controller
)

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='FLASK RESTPLUS API BOILERPLATE WITH JWT',
          version='1.0',
          description='a boilerplate for flask restplus web service')

api.add_namespace(category_ns, path='/category')
api.add_namespace(launch_ns, path='/launches')
api.add_namespace(planning_ns, path='/planning')
api.add_namespace(card_ns, path='/card')
