from random import randint
from bson import ObjectId
from unittest import TestCase

from app.main.service.transformer import category_transformer
from app.main.model.category import Category
from app.main.model.launch import Launch, Setting

class TestCategoryTransofmer(TestCase):
    def test_transform_to_categorized_launches_response(self):
        category1 = generate_category(title='Category 1')
        category2 = generate_category(title='Category 2')
        category3 = generate_category(title='Category 3')

        categories = [category1, category2, category3]
        launches = [
            generate_launch(
                id=str(ObjectId),
                description=f'Launch {index}',
                category_id=category1.id)
            for index in range(3)
        ]

        launches.extend([
            generate_launch(
                id=str(ObjectId),
                description=f'Launch {index}',
                category_id=category2.id)
            for index in range(2)
        ])

        launches.append(
            generate_launch(
                id=str(ObjectId),
                description=f'Final Launch',
                category_id=category3.id)
        )

        response = category_transformer.transform_to_categorized_launches_response(categories, launches)

        self.assertEqual(len(response), 3)

        # =============== CATEGORY 1 ==================
        category = response[0]
        self.assertEqual(category1.id, category.id)
        self.assertEqual(category1.title, category.title)
        self.assertEqual(category1.color, category.color)
        self.assertEqual(category1.category_type, category.type)
        self.assertEqual(category1.planning, category.planning)
        self.assertEqual(len(category.launches), 3)

        launch = category.launches[0]
        self.assertEqual(launches[0].id, launch.id)
        self.assertEqual(launches[0].description, launch.description)
        self.assertEqual(launches[0].value, launch.value)
        self.assertEqual(launches[0].status, launch.status)
        self.assertEqual(launches[0].date, launch.date)

        launch = category.launches[1]
        self.assertEqual(launches[1].id, launch.id)
        self.assertEqual(launches[1].description, launch.description)
        self.assertEqual(launches[1].value, launch.value)
        self.assertEqual(launches[1].status, launch.status)
        self.assertEqual(launches[1].date, launch.date)

        launch = category.launches[2]
        self.assertEqual(launches[2].id, launch.id)
        self.assertEqual(launches[2].description, launch.description)
        self.assertEqual(launches[2].value, launch.value)
        self.assertEqual(launches[2].status, launch.status)
        self.assertEqual(launches[2].date, launch.date)

        # =============== CATEGORY 2 ==================
        category = response[1]
        self.assertEqual(category2.id, category.id)
        self.assertEqual(category2.title, category.title)
        self.assertEqual(category2.color, category.color)
        self.assertEqual(category2.category_type, category.type)
        self.assertEqual(category2.planning, category.planning)
        self.assertEqual(len(category.launches), 2)

        launch = category.launches[0]
        self.assertEqual(launches[3].id, launch.id)
        self.assertEqual(launches[3].description, launch.description)
        self.assertEqual(launches[3].value, launch.value)
        self.assertEqual(launches[3].status, launch.status)
        self.assertEqual(launches[3].date, launch.date)

        launch = category.launches[1]
        self.assertEqual(launches[4].id, launch.id)
        self.assertEqual(launches[4].description, launch.description)
        self.assertEqual(launches[4].value, launch.value)
        self.assertEqual(launches[4].status, launch.status)
        self.assertEqual(launches[4].date, launch.date)

        # =============== CATEGORY 3 ==================
        category = response[2]
        self.assertEqual(category3.id, category.id)
        self.assertEqual(category3.title, category.title)
        self.assertEqual(category3.color, category.color)
        self.assertEqual(category3.category_type, category.type)
        self.assertEqual(category3.planning, category.planning)
        self.assertEqual(len(category.launches), 1)

        launch = category.launches[0]
        self.assertEqual(launches[5].id, launch.id)
        self.assertEqual(launches[5].description, launch.description)
        self.assertEqual(launches[5].value, launch.value)
        self.assertEqual(launches[5].status, launch.status)
        self.assertEqual(launches[5].date, launch.date)


def generate_category(
        id=None,
        title='Category Test',
        color='#fff',
        type='income',
        planning=1000
):
    return Category(
        _id=str(ObjectId()) if id == None else id,
        title=title,
        color=color,
        category_type=type,
        planning=planning,
        position=randint(1, 10)
    )


def generate_launch(
        id=None,
        description='Test',
        date='2023-05',
        status='schedule',
        category_id=ObjectId(),
        launch_id=None,
        option_frequency='unique') -> Launch:
    return Launch(
        _id=id,
        description=description,
        value=1000,
        status=status,
        category_id=category_id,
        date=date,
        setting=Setting(
            _id=ObjectId(),
            launch_id=launch_id,
            option_frequency=option_frequency,
            total_installments=0,
            current_installment=0,
            frequency='Month',
        )
    )
