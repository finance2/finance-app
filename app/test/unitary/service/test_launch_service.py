from copy import copy
from unittest import TestCase
from unittest.mock import Mock, MagicMock

from bson import ObjectId

import app.main.service.launch_service as sut
from app.main.model.launch import Setting, Launch


def generate_data_object(launch_id, date=None, option_frequency='unique', value=1000, current_installment=1,
                         total_installments=0):
    old = Launch(
        _id=launch_id,
        description="Test",
        value=float(value),
        status="scheduled",
        date=date,
        category_id=str(ObjectId()),
        setting=Setting(
            _id=ObjectId(),
            launch_id=launch_id,
            frequency="Mensal",
            option_frequency=option_frequency,
            current_installment=current_installment,
            total_installments=total_installments))

    data = copy(old)
    data.setting = copy(old.setting)
    return old, data

def generate_series(launch_id, end_month, total=0, option_frequency='repeat'):
    series = []
    for index in range(total, 0, -1):
        series.append(generate_data_object(launch_id,
                                           date=f'2021-{end_month:02}-10',
                                           option_frequency=option_frequency,
                                           current_installment=index,
                                           total_installments=total)[0])
        end_month -= 1
    return series

def create_scene_to_update_date_series(launch_id, series, year='2021', month='10', day='10'):
    old_launch, data = generate_data_object(launch_id,
                                            date=f'2021-10-10',
                                            option_frequency='repeat',
                                            current_installment=3,
                                            total_installments=5)
    data.setting = data.setting.__dict__
    data = data.__dict__
    data['date'] = f'{year}-{month}-{day}'

    sut.dao.get_launch = MagicMock(return_value=old_launch)
    sut.dao.get_launches_series = MagicMock(return_value=series)

    return data


class TestLaunchService(TestCase):
    def setUp(self) -> None:
        sut.dao = Mock()

    def test_generate_repeated_launches(self):
        data = {
            "category_id": "6403e930831d334160cf39b4",
            "description": "Cartão",
            "value": 2300,
            "status": "scheduled",
            "date": "2023-04",
            "setting": {
                "option_frequency": "repeat",
                "current_installment": 1,
                "total_installments": 5
            }
        }

        response, status_code = sut.generate_launches(data)
        args, _ = sut.dao.save_launch.call_args
        launches_result = args[1]

        self.assertEqual(5, len(launches_result))

        launch_result = launches_result[0]
        self.assertEqual('2023-04', launch_result.date)
        self.assertEqual(1, launch_result.setting.current_installment)

        launch_result = launches_result[2]
        self.assertEqual('2023-06', launch_result.date)
        self.assertEqual(3, launch_result.setting.current_installment)

        launch_result = launches_result[4]
        self.assertEqual('2023-08', launch_result.date)
        self.assertEqual(5, launch_result.setting.current_installment)

        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully added', response.get('message'))

    def test_generate_parceled_launches(self):
        data = {
            "category_id": "6403e930831d334160cf39b4",
            "description": "Cartão",
            "value": 2000,
            "status": "scheduled",
            "date": "2023-04",
            "setting": {
                "option_frequency": "parcel",
                "current_installment": 1,
                "total_installments": 5
            }
        }

        response, status_code = sut.generate_launches(data)
        args, _ = sut.dao.save_launch.call_args
        launches_result = args[1]

        self.assertEqual(5, len(launches_result))

        launch_result = launches_result[0]
        self.assertEqual(400, launch_result.value)
        self.assertEqual('2023-04', launch_result.date)
        self.assertEqual(1, launch_result.setting.current_installment)

        launch_result = launches_result[2]
        self.assertEqual('2023-06', launch_result.date)
        self.assertEqual(3, launch_result.setting.current_installment)

        launch_result = launches_result[4]
        self.assertEqual('2023-08', launch_result.date)
        self.assertEqual(5, launch_result.setting.current_installment)

        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully added', response.get('message'))

    def test_update_repeat_series_launch(self):
        data = {
            'is_update_all': False,
            'launch': {
                "id": "64232f82c2c4f2832f51280d",
                "description": "Cartão",
                "value": 2800.0,
                "status": "paid",
                "category_id": "6403e930831d334160cf39b4",
                "date": "2023-03",
                "setting": {
                    "_id": "64232f82c2c4f2832f51280e",
                    "launch_id": "64232f82c2c4f2832f51280f",
                    "frequency": "Mensal",
                    "option_frequency": "repeat",
                    "current_installment": 1,
                    "total_installments": 5
                }
            }
        }

        sut.update_launch(data)

        sut.dao.update_all.assert_not_called()
        args, _ = sut.dao.update_launch.call_args
        launch: Launch = args[0]

        self.assertEqual(data.get('launch').get('description'), launch.description)
        self.assertEqual(data.get('launch').get('id'), str(launch._id))
        self.assertEqual(data.get('launch').get('value'), launch.value)
        self.assertEqual(data.get('launch').get('status'), launch.status)
        self.assertEqual(data.get('launch').get('category_id'), str(launch.category_id))
        self.assertEqual(data.get('launch').get('date'), launch.date)
        self.assertEqual(data.get('launch').get('setting').get('_id'), str(launch.setting.id))
        self.assertEqual(data.get('launch').get('setting').get('launch_id'), str(launch.setting.launch_id))
        self.assertEqual(data.get('launch').get('setting').get('frequency'), launch.setting.frequency)
        self.assertEqual(data.get('launch').get('setting').get('current_installment'), launch.setting.current_installment)

    def test_update_repeat_all_series_launch(self):
        data = {
            'is_update_all': True,
            'launch': {
                "id": "64232f82c2c4f2832f51280d",
                "description": "Cartão",
                "value": 2800.0,
                "status": "paid",
                "category_id": "6403e930831d334160cf39b4",
                "date": "2023-03",
                "setting": {
                    "_id": "64232f82c2c4f2832f51280e",
                    "launch_id": "64232f82c2c4f2832f51280f",
                    "frequency": "Mensal",
                    "option_frequency": "repeat",
                    "current_installment": 1,
                    "total_installments": 5
                }
            }
        }

        sut.update_launch(data)

        sut.dao.update_launch.assert_not_called()
        args, _ = sut.dao.update_all.call_args
        launch: Launch = args[0]

        self.assertEqual(data.get('launch').get('description'), launch.description)
        self.assertEqual(data.get('launch').get('id'), str(launch._id))
        self.assertEqual(data.get('launch').get('value'), launch.value)
        self.assertEqual(data.get('launch').get('status'), launch.status)
        self.assertEqual(data.get('launch').get('category_id'), str(launch.category_id))
        self.assertEqual(data.get('launch').get('date'), launch.date)
        self.assertEqual(data.get('launch').get('setting').get('_id'), str(launch.setting.id))
        self.assertEqual(data.get('launch').get('setting').get('launch_id'), str(launch.setting.launch_id))
        self.assertEqual(data.get('launch').get('setting').get('frequency'), launch.setting.frequency)
        self.assertEqual(data.get('launch').get('setting').get('current_installment'), launch.setting.current_installment)
    
    def test_update_parcel_series_launch(self):
        data = {
            'is_update_all': False,
            'launch': {
                "id": "64232f82c2c4f2832f51280d",
                "description": "Cartão",
                "value": 2800.0,
                "status": "paid",
                "category_id": "6403e930831d334160cf39b4",
                "date": "2023-03",
                "setting": {
                    "_id": "64232f82c2c4f2832f51280e",
                    "launch_id": "64232f82c2c4f2832f51280f",
                    "frequency": "Mensal",
                    "option_frequency": "parcel",
                    "current_installment": 1,
                    "total_installments": 5
                }
            }
        }

        sut.update_launch(data)

        sut.dao.update_all.assert_not_called()
        args, _ = sut.dao.update_launch.call_args
        launch: Launch = args[0]

        self.assertEqual(data.get('launch').get('id'), str(launch._id))
        self.assertEqual(data.get('launch').get('description'), launch.description)
        self.assertEqual(data.get('launch').get('value'), launch.value)
        self.assertEqual(data.get('launch').get('status'), launch.status)
        self.assertEqual(data.get('launch').get('category_id'), str(launch.category_id))
        self.assertEqual(data.get('launch').get('date'), launch.date)
        self.assertEqual(data.get('launch').get('setting').get('_id'), str(launch.setting.id))
        self.assertEqual(data.get('launch').get('setting').get('launch_id'), str(launch.setting.launch_id))
        self.assertEqual(data.get('launch').get('setting').get('frequency'), launch.setting.frequency)
        self.assertEqual(data.get('launch').get('setting').get('current_installment'), launch.setting.current_installment)

    def test_update_parcel_all_series_launch(self):
        data = {
            'is_update_all': True,
            'launch': {
                "id": "64232f82c2c4f2832f51280d",
                "description": "Cartão",
                "value": 2800.0,
                "status": "paid",
                "category_id": "6403e930831d334160cf39b4",
                "date": "2023-03",
                "setting": {
                    "_id": "64232f82c2c4f2832f51280e",
                    "launch_id": "64232f82c2c4f2832f51280f",
                    "frequency": "Mensal",
                    "option_frequency": "parcel",
                    "current_installment": 1,
                    "total_installments": 5
                }
            }
        }

        sut.update_launch(data)

        sut.dao.update_launch.assert_not_called()
        args, _ = sut.dao.update_all.call_args
        launch: Launch = args[0]

        self.assertEqual(data.get('launch').get('id'), str(launch._id))
        self.assertEqual(data.get('launch').get('description'), launch.description)
        self.assertEqual(data.get('launch').get('value'), launch.value)
        self.assertEqual(data.get('launch').get('status'), launch.status)
        self.assertEqual(data.get('launch').get('category_id'), str(launch.category_id))
        self.assertEqual(data.get('launch').get('date'), launch.date)
        self.assertEqual(data.get('launch').get('setting').get('_id'), str(launch.setting.id))
        self.assertEqual(data.get('launch').get('setting').get('launch_id'), str(launch.setting.launch_id))
        self.assertEqual(data.get('launch').get('setting').get('frequency'), launch.setting.frequency)
        self.assertEqual(data.get('launch').get('setting').get('current_installment'), launch.setting.current_installment)

    def test_remove_unique_launch(self):
        launch_id = str(ObjectId())
        _, launch = generate_data_object(launch_id, option_frequency='unique')
        sut.dao.find_by_id = MagicMock(return_value=launch)

        sut.remove_launch(launch_id)
        
        sut.dao.remove_launch_by_id.assert_called_once()
        sut.dao.remove_launch_series.assert_not_called()

    def test_remove_repeat_launch(self):
        launch_id = str(ObjectId())
        _, launch = generate_data_object(launch_id, option_frequency='repeat')
        sut.dao.find_by_id = MagicMock(return_value=launch)

        sut.remove_launch(launch_id)

        sut.dao.remove_launch_by_id.assert_not_called()
        sut.dao.remove_launch_series.assert_called_once()

    def test_remove_parcel_launch(self):
        launch_id = str(ObjectId())
        _, launch = generate_data_object(launch_id, option_frequency='parcel')
        sut.dao.find_by_id = MagicMock(return_value=launch)

        sut.remove_launch(launch_id)

        sut.dao.remove_launch_by_id.assert_not_called()
        sut.dao.remove_launch_series.assert_called_once()

    def test_remove_launch_wrong_frequency(self):
        with self.assertRaises(SystemError) as error:
            launch_id = str(ObjectId())
            _, launch = generate_data_object(launch_id, option_frequency='any_value')
            sut.dao.find_by_id = MagicMock(return_value=launch)

            sut.remove_launch(launch)

        sut.dao.remove_launch_by_id.assert_not_called()
        sut.dao.remove_launch_series.assert_not_called()
        self.assertEqual('Function not found!', error.exception.args[0])
