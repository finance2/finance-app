from copy import copy
from unittest import TestCase, skip
from unittest.mock import Mock, ANY, MagicMock

from bson import ObjectId

from app.main.service import planning_service as sut
from app.main.model.launch import Launch, Setting
from app.main.model.planning import Planning


def create_planning_object() -> Planning:
    planning_id = ObjectId()
    return Planning(
        _id=planning_id,
        title='Test',
        color='#fff',
        category_type='incoming',
        planning=15000.0,
        position=1,
        launches=[create_launch(planning_id)[0]],
    )


def create_launch(planning_id, date='2021-10-10', option_frequency='repeat', value=1000, current_installment=1,
                  total_installments=0) -> [Launch, Launch]:
    launch_id = ObjectId()
    old_launch = Launch(
        _id=launch_id,
        description='Launch Test',
        value=float(value),
        status='scheduled',
        category_id=planning_id,
        setting=Setting(
            _id=ObjectId(),
            launch_id=launch_id,
            frequency='Mensal',
            option_frequency=option_frequency,
            current_installment=current_installment,
            total_installments=total_installments
        ),
        date=date
    )

    new_launch = copy(old_launch)
    new_launch.setting = copy(new_launch.setting)
    return old_launch, new_launch


def create_planning_json():
    data = create_planning_object()
    data.launches = [convert_launch_to_json(launch) for launch in data.launches]
    return vars(data)


def convert_launch_to_json(launch: Launch):
    launch.setting = vars(launch.setting)
    return vars(launch)


def generate_series(planning_id, end_month, total=0, option_frequency='repeat'):
    series = []
    for index in range(total, 0, -1):
        series.append(create_launch(planning_id,
                                    date=f'2021-{end_month:02}-10',
                                    option_frequency=option_frequency,
                                    current_installment=index,
                                    total_installments=total)[0])
        end_month -= 1
    return series

@skip('Skiping old tests')
class TestPlanningService(TestCase):
    def setUp(self) -> None:
        sut.planning_dao = Mock()

    def test_create_new_planning(self):
        data = create_planning_json()

        response, status_code = sut.save_planning(data)

        self.assertEqual(201, status_code)
        self.assertEqual('Planning category successfully created', response.get('message'))

    def test_update_planning(self):
        data = create_planning_json()

        response, status_code = sut.update_planning(data)

        self.assertEqual(201, status_code)
        self.assertEqual('Planning category successfully updated', response.get('message'))

    def test_add_unique_launch(self):
        planning_id = ObjectId()
        data = create_launch(planning_id)[0]
        data = convert_launch_to_json(data)

        response, status_code = sut.add_launch(data)

        sut.planning_dao.persist_launches.assert_called_once_with(planning_id, ANY)
        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully registered on planning', response.get('message'))

    def test_add_repeat_launch(self):
        planning_id = ObjectId()
        data = create_launch(planning_id)[0]
        data.date = '2021-12-15'
        data.setting.option_frequency = 'repeat'
        data.setting.current_installment = 1
        data.setting.total_installments = 5
        data = convert_launch_to_json(data)

        response, status_code = sut.add_launch(data)

        launches = sut.planning_dao.persist_launches.call_args[0][1]
        sut.planning_dao.persist_launches.assert_called_once_with(planning_id, ANY)
        self.assertEqual(5, len(launches))
        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully registered on planning', response.get('message'))

    def test_add_parcel_launch(self):
        planning_id = ObjectId()
        data = create_launch(planning_id)[0]
        data.date = '2021-12-15'
        data.setting.option_frequency = 'parcel'
        data.setting.current_installment = 1
        data.setting.total_installments = 5
        data = convert_launch_to_json(data)

        response, status_code = sut.add_launch(data)

        launches = sut.planning_dao.persist_launches.call_args[0][1]
        sut.planning_dao.persist_launches.assert_called_once_with(planning_id, ANY)
        self.assertEqual(5, len(launches))
        self.assertEqual(200.0, launches[0]['value'])
        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully registered on planning', response.get('message'))

    def test_update_unique_launch(self):
        planning_id = ObjectId()
        old_launch, data = create_launch(planning_id)

        data.value = 1500.0
        data = convert_launch_to_json(data)

        sut.planning_dao.get_launch = MagicMock(return_value=old_launch)
        response, status_code = sut.update_launch(data)

        sut.planning_dao.persist_changes.assert_called_once_with(ANY, {'launches.$[launch].value': 1500.0})
        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully updated', response.get('message'))

    def test_update_total_installments_repeat_launch(self):
        planning_id = ObjectId()
        old_launch, data = create_launch(planning_id, total_installments=5)
        series = generate_series(planning_id, end_month=12, total=5)

        data.date = '2021-10-15'
        data.setting.total_installments = 3
        data = convert_launch_to_json(data)

        sut.planning_dao.get_launch = MagicMock(return_value=old_launch)
        sut.planning_dao.get_launches_series = MagicMock(return_value=series)
        response, status_code = sut.update_launch(data)

        sut.planning_dao.persist_update_launch_date.assert_called()
        sut.planning_dao.remove_launches_by_index.assert_called_once_with(old_launch, 3)
        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully updated', response.get('message'))

        month = 12
        for value in series:
            self.assertEqual(f'2021-{month:02}-15', value.date)
            month -= 1

    def test_update_to_more_total_installments_repeat_launch(self):
        planning_id = ObjectId()
        old_launch, data = create_launch(planning_id, total_installments=5)
        series = generate_series(planning_id, end_month=12, total=5)

        data.setting.total_installments = 10
        data = convert_launch_to_json(data)

        sut.planning_dao.get_launch = MagicMock(return_value=old_launch)
        sut.planning_dao.get_launches_series = MagicMock(return_value=series)
        response, status_code = sut.update_launch(data)

        launches = sut.planning_dao.persist_launches.call_args[0][1]
        sut.planning_dao.persist_launches.assert_called_once_with(planning_id, launches)
        self.assertEqual(5, len(launches))
        self.assertEqual(201, status_code)
        self.assertEqual('Launch successfully updated', response.get('message'))
