import uuid
from unittest import TestCase
from unittest.mock import (
    Mock, MagicMock
)

from bson import ObjectId

import app.main.service.card_service as service
from app.main.model.card_launch import CardLaunch


class TestCardService(TestCase):
    def setUp(self):
        service.launch_repository = Mock()

    def test_register_unique_card_launch(self):
        data = {
            'description': 'Lançamento único',
            'value': 50,
            'category': 'Lanche',
            'date': '2024-04-20',
            'card_id': uuid.uuid4(),
            'setting': {
                'frequency': 'mensal',
                "option_frequency": "unique",
                "current_installment": 0,
                "total_installments": 1
            }
        }
        service.add_card_launch(data)

        args, _ = service.launch_repository.save_card_launch.call_args

        launches = args[0]
        self.assertEqual(1, len(launches))

        launch: CardLaunch = launches[0]
        self.assertEqual(data.get('description'), launch.description)
        self.assertEqual(data.get('value'), launch.value)
        self.assertEqual(data.get('category'), launch.category)
        self.assertEqual(data.get('date'), launch.date)
        self.assertEqual(data.get('card_id'), launch.card_id)

        setting = data.get('setting')
        self.assertEqual(setting.get('frequency'), launch.setting.frequency)
        self.assertEqual(setting.get('option_frequency'), launch.setting.option_frequency)
        self.assertEqual(setting.get('current_installment'), launch.setting.current_installment)
        self.assertEqual(setting.get('total_installments'), launch.setting.total_installments)
        self.assertEqual(str(launch.id), launch.setting.launch_id)

    def test_register_parcel_card_launch(self):
        data = {
            'description': 'Lançamento repetido',
            'value': 50,
            'category': 'Lanche',
            'date': '2024-04-20',
            'card_id': uuid.uuid4(),
            'setting': {
                'frequency': 'mensal',
                "option_frequency": "parcel",
                "current_installment": 1,
                "total_installments": 3
            }
        }
        service.add_card_launch(data)

        args, _ = service.launch_repository.save_card_launch.call_args
        launches = args[0]

        self.assertEqual(3, len(launches))

        launch: CardLaunch = launches[0]
        self.assertEqual(data.get('description'), launch.description)
        self.assertEqual(data.get('value'), launch.value)
        self.assertEqual(data.get('category'), launch.category)
        self.assertEqual(data.get('date'), launch.date)
        self.assertEqual(data.get('card_id'), launch.card_id)

        setting = data.get('setting')
        self.assertEqual(setting.get('frequency'), launch.setting.frequency)
        self.assertEqual(setting.get('option_frequency'), launch.setting.option_frequency)
        self.assertEqual(setting.get('current_installment'), launch.setting.current_installment)
        self.assertEqual(setting.get('total_installments'), launch.setting.total_installments)
        self.assertEqual(str(launch.id), launch.setting.launch_id)

        main_launch_id = launch.id
        launch: CardLaunch = launches[1]
        self.assertEqual(data.get('value'), launch.value)
        self.assertEqual('2024-05-20', launch.date)
        self.assertEqual(2, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)

        launch: CardLaunch = launches[2]
        self.assertEqual(data.get('value'), launch.value)
        self.assertEqual('2024-06-20', launch.date)
        self.assertEqual(3, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)

    def test_register_repeat_card_launch(self):
        data = {
            'description': 'Lançamento repetido',
            'value': 50,
            'category': 'Lanche',
            'date': '2024-04-20',
            'card_id': uuid.uuid4(),
            'setting': {
                'frequency': 'mensal',
                "option_frequency": "repeat",
                "current_installment": 1,
                "total_installments": 3
            }
        }
        service.add_card_launch(data)

        args, _ = service.launch_repository.save_card_launch.call_args
        launches = args[0]

        self.assertEqual(3, len(launches))

        launch: CardLaunch = launches[0]
        self.assertEqual(data.get('description'), launch.description)
        self.assertEqual(data.get('value'), launch.value)
        self.assertEqual(data.get('category'), launch.category)
        self.assertEqual(data.get('date'), launch.date)
        self.assertEqual(data.get('card_id'), launch.card_id)

        setting = data.get('setting')
        self.assertEqual(setting.get('frequency'), launch.setting.frequency)
        self.assertEqual(setting.get('option_frequency'), launch.setting.option_frequency)
        self.assertEqual(setting.get('current_installment'), launch.setting.current_installment)
        self.assertEqual(setting.get('total_installments'), launch.setting.total_installments)
        self.assertEqual(str(launch.id), launch.setting.launch_id)

        main_launch_id = launch.id
        launch: CardLaunch = launches[1]
        self.assertEqual(data.get('value'), launch.value)
        self.assertEqual('2024-05-20', launch.date)
        self.assertEqual(2, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)

        launch: CardLaunch = launches[2]
        self.assertEqual(data.get('value'), launch.value)
        self.assertEqual('2024-06-20', launch.date)
        self.assertEqual(3, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)

    def test_update_card_launch(self):
        data = {
            'id': str(ObjectId()),
            'description': 'Updating card launch',
            'value': 1000.0,
            'category': 'test',
            'setting': {
                'launch_id': str(ObjectId())
            }
        }

        service.update_launch_card(data)

        args, _ = service.launch_repository.update_card_launch.call_args

        card_launch: CardLaunch = args[0]

        self.assertEqual(data.get('value'), card_launch.value)
        self.assertEqual(data.get('category'), card_launch.category)
        self.assertEqual(data.get('description'), card_launch.description)
        self.assertEqual(data.get('setting').get('launch_id'), str(card_launch.setting.launch_id))

    def test_remove_card_and_launches(self):
        card_id = str(ObjectId())

        service.repository.remove_card = MagicMock()
        service.launch_repository.remove_card_launches_by_card = MagicMock()

        service.remove_card(card_id)

        service.repository.remove_card.assert_called_once_with(card_id)
        service.launch_repository.remove_card_launches_by_card.assert_called_once_with(card_id)
