from random import randint
from bson import ObjectId
from unittest import TestCase
from unittest.mock import Mock, MagicMock

import app.main.service.category_service as service
from app.main.model.launch import Launch, Setting
from app.main.model.category import Category

def generate_data(
        id=str(ObjectId()),
        title='Category Test',
        color='#fff',
        type='income',
        planning=1000
):
    return {
        'id': id,
        'title': title,
        'color': color,
        'category_type': type,
        'planning': planning,
        'position': 1
    }


def generate_category(
        id=None,
        title='Category Test',
        color='#fff',
        type='income',
        planning=1000
):
    return Category(
        _id=str(ObjectId()) if id == None else id,
        title=title,
        color=color,
        category_type=type,
        planning=planning,
        position=randint(1, 10)
    )

def generate_launch(
        id=None,
        description='Test',
        date='2023-05',
        status='schedule',
        category_id=ObjectId(),
        launch_id=None,
        option_frequency='unique') -> Launch:
    return Launch(
        _id=id,
        description=description,
        value=1000,
        status=status,
        category_id=category_id,
        date=date,
        setting=Setting(
            _id=ObjectId(),
            launch_id=launch_id,
            option_frequency=option_frequency,
            total_installments=0,
            current_installment=0,
            frequency='Month',
        )
    )

class TestCategoryService(TestCase):
    def setUp(self) -> None:
        service.category_dao = Mock()

    def test_create_category(self):
        data = generate_data()

        response, status_code = service.create_category(data)

        args, _ = service.category_dao.insert_category.call_args
        category: Category = args[0]

        self.assertEqual(data.get('id'), str(category.id))
        self.assertEqual(data.get('title'), category.title)
        self.assertEqual(data.get('color'), category.color)
        self.assertEqual(data.get('category_type'), category.category_type)
        self.assertEqual(data.get('planning'), category.planning)
        self.assertEqual(data.get('position'), category.position)

        self.assertEqual(201, status_code)
        self.assertEqual('success', response.get('status'))
        self.assertEqual('Category successfully registered', response.get('message'))
    
    def test_update_category(self):
        data = generate_data()

        response, status_code = service.update_category(data)

        args, _ = service.category_dao.update_category.call_args
        category: Category = args[0]

        self.assertEqual(data.get('id'), str(category.id))
        self.assertEqual(data.get('title'), category.title)
        self.assertEqual(data.get('color'), category.color)
        self.assertEqual(data.get('category_type'), category.category_type)
        self.assertEqual(data.get('planning'), category.planning)
        self.assertEqual(data.get('position'), category.position)

        self.assertEqual(201, status_code)
        self.assertEqual('success', response.get('status'))
        self.assertEqual('Category successfully updated', response.get('message'))
    
    def test_extract_categories_from_list_launches(self):
        category1 = generate_category(title='Category 1')
        category2 = generate_category(title='Category 2')
        category3 = generate_category(title='Category 3')

        launches = [
            generate_launch(
                id=str(ObjectId),
                description=f'Launch {index}',
                category_id=category1.id)
            for index in range(3)
        ]

        launches.extend([
            generate_launch(
                id=str(ObjectId),
                description=f'Launch {index}',
                category_id=category2.id)
            for index in range(2)
        ])

        launches.append(
            generate_launch(
                id=str(ObjectId),
                description=f'Final Launch',
                category_id=category3.id)
        )

        categories = service._extract_categories(launches)

        self.assertEqual(len(categories), 3)
        self.assertEqual(category1.id, categories[0])
        self.assertEqual(category2.id, categories[1])
        self.assertEqual(category3.id, categories[2])
    
    def test_list_launches_categorized(self):
        category1 = generate_category(title='Category 1')
        category2 = generate_category(title='Category 2')
        category3 = generate_category(title='Category 3')

        launches = [
            generate_launch(
                id=str(ObjectId),
                description=f'Launch {index}',
                category_id=category1.id)
            for index in range(3)
        ]

        launches.extend([
            generate_launch(
                id=str(ObjectId),
                description=f'Launch {index}',
                category_id=category2.id)
            for index in range(2)
        ])

        launches.append(
            generate_launch(
                id=str(ObjectId),
                description=f'Final Launch',
                category_id=category3.id)
        )
        
        service.launch_service.dao.get_launches_by_month_year = MagicMock(return_value=launches)
        service.category_dao.find_by_list_ids = MagicMock(return_value=[category1, category2, category3])

        categories = service.list_from_month_year('2023-05')

        self.assertEqual(len(categories), 3)

        # =============== CATEGORY 1 ==================
        category = categories[0]
        self.assertEqual(category1.id, category.id)
        self.assertEqual(category1.title, category.title)
        self.assertEqual(category1.color, category.color)
        self.assertEqual(category1.category_type, category.type)
        self.assertEqual(category1.planning, category.planning)
        self.assertEqual(len(category.launches), 3)

        launch = category.launches[0]
        self.assertEqual(launches[0].id, launch.id)
        self.assertEqual(launches[0].description, launch.description)
        self.assertEqual(launches[0].value, launch.value)
        self.assertEqual(launches[0].status, launch.status)
        self.assertEqual(launches[0].date, launch.date)

        launch = category.launches[1]
        self.assertEqual(launches[1].id, launch.id)
        self.assertEqual(launches[1].description, launch.description)
        self.assertEqual(launches[1].value, launch.value)
        self.assertEqual(launches[1].status, launch.status)
        self.assertEqual(launches[1].date, launch.date)

        launch = category.launches[2]
        self.assertEqual(launches[2].id, launch.id)
        self.assertEqual(launches[2].description, launch.description)
        self.assertEqual(launches[2].value, launch.value)
        self.assertEqual(launches[2].status, launch.status)
        self.assertEqual(launches[2].date, launch.date)

        # =============== CATEGORY 2 ==================
        category = categories[1]
        self.assertEqual(category2.id, category.id)
        self.assertEqual(category2.title, category.title)
        self.assertEqual(category2.color, category.color)
        self.assertEqual(category2.category_type, category.type)
        self.assertEqual(category2.planning, category.planning)
        self.assertEqual(len(category.launches), 2)

        launch = category.launches[0]
        self.assertEqual(launches[3].id, launch.id)
        self.assertEqual(launches[3].description, launch.description)
        self.assertEqual(launches[3].value, launch.value)
        self.assertEqual(launches[3].status, launch.status)
        self.assertEqual(launches[3].date, launch.date)

        launch = category.launches[1]
        self.assertEqual(launches[4].id, launch.id)
        self.assertEqual(launches[4].description, launch.description)
        self.assertEqual(launches[4].value, launch.value)
        self.assertEqual(launches[4].status, launch.status)
        self.assertEqual(launches[4].date, launch.date)

        # =============== CATEGORY 3 ==================
        category = categories[2]
        self.assertEqual(category3.id, category.id)
        self.assertEqual(category3.title, category.title)
        self.assertEqual(category3.color, category.color)
        self.assertEqual(category3.category_type, category.type)
        self.assertEqual(category3.planning, category.planning)
        self.assertEqual(len(category.launches), 1)

        launch = category.launches[0]
        self.assertEqual(launches[5].id, launch.id)
        self.assertEqual(launches[5].description, launch.description)
        self.assertEqual(launches[5].value, launch.value)
        self.assertEqual(launches[5].status, launch.status)
        self.assertEqual(launches[5].date, launch.date)
