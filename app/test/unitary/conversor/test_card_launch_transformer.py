from unittest import TestCase

from bson import ObjectId

from app.main.conversor import (
    card_launch_transformer as transformer
)
from app.main.model.card_launch import CardLaunch
from app.main.service.dto.request.card_launch.add_card_launch_request import AddCardLaunchRequest, Setting


class TestCardLaunchTransformer(TestCase):

    def test_generate_repeated_launches_from_request(self):
        request = AddCardLaunchRequest(
            description='Lançamento repetido',
            value=50,
            category='Lanche',
            date='2024-04-20',
            card_id=str(ObjectId()),
            setting=Setting(
                frequency='mensal',
                option_frequency='repeat',
                current_installment=1,
                total_installments=3
            )
        )

        launches = transformer.generate_repeated_launches(request)

        self.assertEqual(3, len(launches))

        launch: CardLaunch = launches[0]
        self.assertEqual(request.description, launch.description)
        self.assertEqual(request.value, launch.value)
        self.assertEqual(request.category, launch.category)
        self.assertEqual(request.date, launch.date)
        self.assertEqual(request.card_id, launch.card_id)

        setting = request.setting
        self.assertEqual(setting.frequency, launch.setting.frequency)
        self.assertEqual(setting.option_frequency, launch.setting.option_frequency)
        self.assertEqual(setting.current_installment, launch.setting.current_installment)
        self.assertEqual(setting.total_installments, launch.setting.total_installments)
        self.assertEqual(str(launch.id), launch.setting.launch_id)

        main_launch_id = launch.id
        launch: CardLaunch = launches[1]
        self.assertEqual(request.value, launch.value)
        self.assertEqual('2024-05-20', launch.date)
        self.assertEqual(2, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)

        launch: CardLaunch = launches[2]
        self.assertEqual(request.value, launch.value)
        self.assertEqual('2024-06-20', launch.date)
        self.assertEqual(3, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)

    def test_generate_parcel_launches_from_request(self):
        request = AddCardLaunchRequest(
            description='Lançamento repetido',
            value=150,
            category='Lanche',
            date='2024-04-20',
            card_id=str(ObjectId()),
            setting=Setting(
                frequency='mensal',
                option_frequency='parcel',
                current_installment=1,
                total_installments=3
            )
        )

        launches = transformer.generate_repeated_launches(request)

        self.assertEqual(3, len(launches))

        launch: CardLaunch = launches[0]
        self.assertEqual(request.description, launch.description)
        self.assertEqual(request.value, launch.value)
        self.assertEqual(request.category, launch.category)
        self.assertEqual(request.date, launch.date)
        self.assertEqual(request.card_id, launch.card_id)

        setting = request.setting
        self.assertEqual(setting.frequency, launch.setting.frequency)
        self.assertEqual(setting.option_frequency, launch.setting.option_frequency)
        self.assertEqual(setting.current_installment, launch.setting.current_installment)
        self.assertEqual(setting.total_installments, launch.setting.total_installments)
        self.assertEqual(str(launch.id), launch.setting.launch_id)

        main_launch_id = launch.id
        launch: CardLaunch = launches[1]
        self.assertEqual(request.value, launch.value)
        self.assertEqual('2024-05-20', launch.date)
        self.assertEqual(2, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)

        launch: CardLaunch = launches[2]
        self.assertEqual(request.value, launch.value)
        self.assertEqual('2024-06-20', launch.date)
        self.assertEqual(3, launch.setting.current_installment)
        self.assertEqual(str(main_launch_id), launch.setting.launch_id)
