from unittest import TestCase, main
from unittest.mock import Mock, MagicMock

from bson import ObjectId
from app.main.conversor import launch_conversor

from app.main.dao import launch_dao as dao
from app.main.model.launch import Launch, Setting


class TestLaunchDao(TestCase):
    def setUp(self) -> None:
        dao.mongo.db = Mock()

    def test_update_launch(self):
        launch = create_launch()
        dao.mongo.db.category.aggregate = MagicMock()
        dao.update_launch(launch)

    def test_duplicate_unique_launches(self):
        month_year = '2023-03'
        launch = create_launch(id=ObjectId())
        launch1 = launch_conversor.launch_to_dict(launch)
        launch3 = launch_conversor.launch_to_dict(create_launch(option_frequency='repeat'))
        launch2 = launch_conversor.launch_to_dict(create_launch(launch_id=launch.id, date='2023-03', status='paid'))
        dao.mongo.db.launch.find = MagicMock(return_value=[launch1, launch2, launch3])

        result = dao.get_launches_by_month_year(month_year)
        self.assertEqual(2, len(result))
        self.assertEqual('paid', result[0].status)

    def test_remove_launch_by_id(self):
        launch = create_launch(ObjectId())
        dao.remove_launch_by_id(launch)
        dao.mongo.db.launch.delete_one.assert_called_once()
    
    def test_remove_launch_series(self):
        launch = create_launch(ObjectId(), option_frequency='repeat')
        dao.remove_launch_series(launch)
        dao.mongo.db.launch.delete_many.assert_called_once()


if __name__ == '__main__':
    main()


def create_launch(
        id=None,
        date=None,
        status='schedule',
        category_id=ObjectId(),
        launch_id=None,
        option_frequency='unique') -> Launch:
    id = ObjectId()
    return Launch(
        _id=id,
        description='Test',
        value=1000,
        status=status,
        category_id=category_id,
        date=date,
        setting=Setting(
            _id=ObjectId(),
            launch_id=id if launch_id is None else launch_id,
            option_frequency=option_frequency,
            total_installments=0,
            current_installment=0,
            frequency='Month',
        )
    )
