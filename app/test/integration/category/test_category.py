import pytest

from random import randint
from bson import ObjectId

from app.test.integration.category.pages.category_page import CategoryPage
from app.test.integration.launch.pages.launch_page import LaunchPage

category_id = str(ObjectId())

@pytest.mark.integration
def test_create_category(category_page: CategoryPage):
    data = generate_data(id=category_id)
    
    response = category_page.create(data)

    assert response.status_code == 201
    assert response.json.get('message') == 'Category successfully registered'

@pytest.mark.integration
def test_update_category(category_page: CategoryPage):
    data = generate_data(id=category_id)

    response = category_page.update(data)

    assert response.status_code == 201
    assert response.json.get('message') == 'Category successfully updated'

@pytest.mark.integration
def test_list_launches_categorized(category_page: CategoryPage, launch_page: LaunchPage):
    month_year = '2023-04'

    data_category1 = generate_data(title='Category 1')
    data_category2 = generate_data(title='Category 2')
    data_category3 = generate_data(title='Category 3')

    category_page.create(data_category1)
    category_page.create(data_category2)
    category_page.create(data_category3)

    for index in range(3):
        launch = generate_data_launch(date=month_year, category_id=data_category1.get('id'), description=f'Launch {index}')
        launch_page.create(launch)
    
    for index in range(3, 5):
        launch = generate_data_launch(date=month_year, category_id=data_category2.get('id'), description=f'Launch {index}')
        launch_page.create(launch)

    launch = generate_data_launch(date=month_year, category_id=data_category3.get('id'), description=f'Launch {index}')
    launch_page.create(launch)

    response = category_page.launches_categorized_by_month_year(month_year)

    assert response.status_code == 200

    categories = response.json
    assert len(categories) == 3
    
    assert categories[0].get('id') == data_category1.get('id')
    assert categories[0].get('title') == data_category1.get('title')
    assert categories[0].get('color') == data_category1.get('color')
    assert categories[0].get('type') == data_category1.get('category_type')
    assert categories[0].get('planning') == data_category1.get('planning')
    assert categories[0].get('position') == data_category1.get('position')
    assert len(categories[0].get('launches')) == 3
    
    assert categories[1].get('id') == data_category2.get('id')
    assert categories[1].get('title') == data_category2.get('title')
    assert categories[1].get('color') == data_category2.get('color')
    assert categories[1].get('type') == data_category2.get('category_type')
    assert categories[1].get('planning') == data_category2.get('planning')
    assert categories[1].get('position') == data_category2.get('position')
    assert len(categories[1].get('launches')) == 2

    assert categories[2].get('id') == data_category3.get('id')
    assert categories[2].get('title') == data_category3.get('title')
    assert categories[2].get('color') == data_category3.get('color')
    assert categories[2].get('type') == data_category3.get('category_type')
    assert categories[2].get('planning') == data_category3.get('planning')
    assert categories[2].get('position') == data_category3.get('position')
    assert len(categories[2].get('launches')) == 1


def generate_data(
        id=None,
        title='Category Test',
        color='#fff',
        type='income',
        planning=1000
):
    return {
        'id': str(ObjectId()) if id == None else id,
        'title': title,
        'color': color,
        'category_type': type,
        'planning': planning,
        'position': randint(0, 10)
    }


def generate_data_launch(
        description: str,
        value=1000,
        status='schedule',
        category_id=None,
        date=None,
        launch_id=None,
        frequency='Mensal',
        option_frequency='unique',
        current_installment=0,
        total_installments=0):

    setting = {
        key: value for key, value in dict(
            launch_id=launch_id,
            frequency=frequency,
            option_frequency=option_frequency,
            current_installment=current_installment,
            total_installments=total_installments
        ).items()
        if value is not None
    }

    launch = dict(
        description=description,
        value=value,
        status=status,
        category_id=str(ObjectId()) if category_id == None else category_id,
        date=date,
        setting=setting
    )

    return {
        key: value for key, value in launch.items()
        if value is not None
    }
