import json

from flask.testing import FlaskClient

from app.main import mongo

class CategoryPage:
    def __init__(self, client: FlaskClient) -> None:
        self.client = client

    def create(self, data):
        return self.client.post('/category/', data = json.dumps(data), content_type = 'application/json')

    def update(self, data):
        return self.client.put('/category/', data = json.dumps(data), content_type='application/json')
    
    def launches_categorized_by_month_year(self, month_year: str):
        return self.client.get(f'/category/{month_year}')

    def clear_all(self):
        mongo.db.category.delete_many({})