import pytest
import manage
from flask import Flask

from app.test.integration.launch.pages.launch_page import LaunchPage
from app.test.integration.category.pages.category_page import CategoryPage


@pytest.fixture(scope='module')
def app():
    """Instance main app"""
    return manage.app

@pytest.fixture(scope='module')
def launch_page(app: Flask):
    launch_page = LaunchPage(app.test_client())
    yield launch_page

    launch_page.clear_all()

@pytest.fixture(scope='module')
def category_page(app: Flask):
    category_page = CategoryPage(app.test_client())
    yield category_page

    category_page.clear_all()    
