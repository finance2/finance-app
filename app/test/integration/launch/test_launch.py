
import pytest

from bson import ObjectId

from app.main.conversor import launch_conversor
from app.test.integration.launch.pages.launch_page import LaunchPage

month_year = '2023-03'

@pytest.mark.integration
def test_create_unique_launch(launch_page: LaunchPage):
    launch = _build_launch(description="Launch Unique")
    response = launch_page.create(launch)

    assert response.status_code == 201
    assert response.json.get('message') == 'Launch successfully added'

@pytest.mark.integration
def test_create_repeat_launch(launch_page: LaunchPage):
    launch = _build_launch(
        description="Launch Repeat",
        current_installment=1,
        total_installments=3,
        option_frequency='repeat',
        date=month_year)
    response = launch_page.create(launch)

    assert response.status_code == 201
    assert response.json.get('message') == 'Launch successfully added'
    assert ObjectId.is_valid(response.json.get('id'))

@pytest.mark.integration
def test_create_parcel_launch(launch_page: LaunchPage):
    launch = _build_launch(
        description="Launch Parcel",
        current_installment=1,
        total_installments=3,
        option_frequency='parcel',
        date=month_year)
    response = launch_page.create(launch)

    assert response.status_code == 201
    assert response.json.get('message') == 'Launch successfully added'

@pytest.mark.integration
def test_list_launches_by_past_month(launch_page: LaunchPage):
    response = launch_page.launches_by_month('2023-02')

    assert len(response.json) == 1
    launch = launch_conversor.dict_to_launch(response.json[0])
    assert launch.description == 'Launch Unique'
    assert launch.status == 'schedule'
    assert launch.date == None
    assert launch.setting.option_frequency == 'unique'
    assert launch.setting.current_installment == 0
    assert launch.setting.total_installments == 0

@pytest.mark.integration
def test_list_launches_by_current_month(launch_page: LaunchPage):
    response = launch_page.launches_by_month(month_year)

    assert len(response.json) == 3
    launch = launch_conversor.dict_to_launch(response.json[0])
    assert launch.description == 'Launch Unique'
    assert launch.status == 'schedule'
    assert launch.date == None
    assert launch.setting.option_frequency == 'unique'
    assert launch.setting.current_installment == 0
    assert launch.setting.total_installments == 0

    launch = launch_conversor.dict_to_launch(response.json[1])
    assert launch.description == 'Launch Repeat'
    assert launch.status == 'schedule'
    assert launch.date == month_year
    assert launch.setting.option_frequency == 'repeat'
    assert launch.setting.current_installment == 1
    assert launch.setting.total_installments == 3

    launch = launch_conversor.dict_to_launch(response.json[2])
    assert launch.description == 'Launch Parcel'
    assert launch.status == 'schedule'
    assert launch.date == month_year
    assert launch.setting.option_frequency == 'parcel'
    assert launch.setting.current_installment == 1
    assert launch.setting.total_installments == 3

@pytest.mark.integration
def test_list_launches_by_future_month(launch_page: LaunchPage):
    response = launch_page.launches_by_month('2023-05')

    assert len(response.json) == 3
    launch = launch_conversor.dict_to_launch(response.json[0])
    assert launch.description == 'Launch Unique'
    assert launch.status == 'schedule'
    assert launch.date == None
    assert launch.setting.option_frequency == 'unique'
    assert launch.setting.current_installment == 0
    assert launch.setting.total_installments == 0

    launch = launch_conversor.dict_to_launch(response.json[1])
    assert launch.description == 'Launch Repeat'
    assert launch.status == 'schedule'
    assert launch.date == '2023-05'
    assert launch.setting.option_frequency == 'repeat'
    assert launch.setting.current_installment == 3
    assert launch.setting.total_installments == 3

    launch = launch_conversor.dict_to_launch(response.json[2])
    assert launch.description == 'Launch Parcel'
    assert launch.status == 'schedule'
    assert launch.date == '2023-05'
    assert launch.setting.option_frequency == 'parcel'
    assert launch.setting.current_installment == 3
    assert launch.setting.total_installments == 3

@pytest.mark.integration
def test_list_launches_by_future_month_without_parcel_and_repeat(launch_page: LaunchPage):
    response = launch_page.launches_by_month('2023-06')

    assert len(response.json) == 1
    launch = launch_conversor.dict_to_launch(response.json[0])
    assert launch.description == 'Launch Unique'
    assert launch.status == 'schedule'
    assert launch.date == None
    assert launch.setting.option_frequency == 'unique'
    assert launch.setting.current_installment == 0
    assert launch.setting.total_installments == 0

@pytest.mark.integration
def test_update_unique_launch(launch_page: LaunchPage):
    response = launch_page.launches_by_month(month_year)
    launch = launch_conversor.dict_to_launch(response.json[0])
    launch_id = str(launch.id)

    launch = _build_launch(
        id=launch_id,
        description='Launch Updated',
        value=launch.value,
        status=launch.status,
        category_id=str(launch.category_id),
        date=month_year,
        launch_id=str(launch.setting.launch_id),
        frequency=launch.setting.frequency,
        option_frequency=launch.setting.option_frequency,
        current_installment=launch.setting.current_installment,
        total_installments=launch.setting.total_installments
    )

    response = launch_page.update({
        'isUpdateAll': False,
        'launch': launch
    })

    assert response.status_code == 200
    assert response.json.get('message') == 'Launch successfully updated'
    assert response.json.get('id') == launch_id

    response = launch_page.launches_by_month(month_year)
    assert response.status_code == 200
    assert len(response.json) == 3

    launch = launch_conversor.dict_to_launch(response.json[2])
    assert launch.description == 'Launch Updated'

    response = launch_page.launches_by_month('2023-05')
    assert response.status_code == 200
    assert len(response.json) == 3

    launch = launch_conversor.dict_to_launch(response.json[0])
    assert launch.description == 'Launch Unique'

@pytest.mark.integration
def test_update_unique_replication_launch(launch_page: LaunchPage):
    response = launch_page.launches_by_month(month_year)
    launch = launch_conversor.dict_to_launch(response.json[2])
    launch_id = str(launch.id)

    launch = _build_launch(
        id=launch_id,
        description='Launch Update Replication',
        value=launch.value,
        status=launch.status,
        category_id=str(launch.category_id),
        date=month_year,
        launch_id=str(launch.setting.launch_id),
        frequency=launch.setting.frequency,
        option_frequency=launch.setting.option_frequency,
        current_installment=launch.setting.current_installment,
        total_installments=launch.setting.total_installments
    )

    response = launch_page.update({
        'isUpdateAll': False,
        'launch': launch
    })

    assert response.status_code == 200
    assert response.json.get('message') == 'Launch successfully updated'
    assert response.json.get('id') == launch_id

    response = launch_page.launches_by_month(month_year)
    assert response.status_code == 200
    assert len(response.json) == 3

    launch = launch_conversor.dict_to_launch(response.json[2])
    assert launch.description == 'Launch Update Replication'

    response = launch_page.launches_by_month('2023-05')
    assert response.status_code == 200
    assert len(response.json) == 3

    launch = launch_conversor.dict_to_launch(response.json[0])
    assert launch.description == 'Launch Unique'



@pytest.mark.integration
def test_update_serie_unique_launch(launch_page: LaunchPage):
    response = launch_page.launches_by_month(month_year)
    launch = launch_conversor.dict_to_launch(response.json[0])
    launch_id = str(launch.id)

    launch = _build_launch(
        id=launch_id,
        description='Launch Updated',
        value=launch.value,
        status=launch.status,
        category_id=str(launch.category_id),
        date=launch.date,
        launch_id=str(launch.setting.launch_id),
        frequency=launch.setting.frequency,
        option_frequency=launch.setting.option_frequency,
        current_installment=launch.setting.current_installment,
        total_installments=launch.setting.total_installments
    )

    response = launch_page.update({
        'isUpdateAll': True,
        'launch': launch
    })

    assert response.status_code == 200
    assert response.json.get('message') == 'Launch successfully updated'
    assert response.json.get('id') == launch_id

    response = launch_page.launch_by_id(launch_id)
    assert response.status_code == 200

    launch = launch_conversor.dict_to_launch(response.json)
    assert launch.description == 'Launch Updated'


@pytest.mark.integration
def test_remove_unique_launch_by_id(launch_page: LaunchPage):
    response = launch_page.launches_by_month(month_year)
    response = next(filter(lambda value: value['setting']['option_frequency'] == 'unique', response.json))
    launch = launch_conversor.dict_to_launch(response)

    launch_page.remove_by_id(launch.id)

    response = launch_page.launches_by_month(month_year)
    response = list(map(launch_conversor.dict_to_launch, response.json))
    result = [value for value in response if value.id == launch.id]
    assert len(result) == 0


@pytest.mark.integration
def test_remove_parcel_launch_by_id(launch_page: LaunchPage):
    response = launch_page.launches_by_month(month_year)
    response = next(filter(
        lambda value: value['setting']['option_frequency'] == 'parcel', response.json))
    launch = launch_conversor.dict_to_launch(response)

    launch_page.remove_by_id(launch.id)

    response = launch_page.launches_by_month(month_year)
    response = list(map(launch_conversor.dict_to_launch, response.json))
    result = [value for value in response if value.setting.launch_id == launch.setting.launch_id]
    assert len(result) == 0

    response = launch_page.launches_by_month('2023-04')
    response = list(map(launch_conversor.dict_to_launch, response.json))
    result = [value for value in response if value.setting.launch_id == launch.setting.launch_id]
    assert len(result) == 0


@pytest.mark.integration
def test_remove_repeat_launch_by_id(launch_page: LaunchPage):
    response = launch_page.launches_by_month(month_year)
    response = next(filter(
        lambda value: value['setting']['option_frequency'] == 'repeat', response.json))
    launch = launch_conversor.dict_to_launch(response)

    launch_page.remove_by_id(launch.id)

    response = launch_page.launches_by_month(month_year)
    response = list(map(launch_conversor.dict_to_launch, response.json))
    result = [value for value in response if value.setting.launch_id == launch.setting.launch_id]
    assert len(result) == 0

    response = launch_page.launches_by_month('2023-04')
    response = list(map(launch_conversor.dict_to_launch, response.json))
    result = [value for value in response if value.setting.launch_id == launch.setting.launch_id]
    assert len(result) == 0

def _build_launch(
    description: str,
    id = None,
    value = 1000,
    status = 'schedule',
    category_id = str(ObjectId()),
    date = None,
    launch_id = None,
    frequency = 'Mensal',
    option_frequency = 'unique',
    current_installment = 0,
    total_installments = 0):

    setting = {
        key: value for key, value in dict(
            launch_id = launch_id,
            frequency = frequency,
            option_frequency = option_frequency,
            current_installment = current_installment,
            total_installments = total_installments
        ).items()
        if value is not None
    }

    launch = dict(
        id = id,
        description = description,
        value = value,
        status = status,
        category_id = category_id,
        date = date,
        setting = setting
    )

    return {
        key: value for key, value in launch.items()
        if value is not None
    }
