import json
from flask.testing import FlaskClient

from app.main import mongo

class LaunchPage:
    
    def __init__(self, client: FlaskClient) -> None:
        self.client = client
    
    def create(self, data: dict):
        return self.client.post('/launches/', data = json.dumps(data), content_type='application/json')
    
    def update(self, data: dict):
        return self.client.put('/launches/', data = json.dumps(data), content_type='application/json')
    
    def launches_by_month(self, month_year: str):
        return self.client.get(f'/launches/?monthYear={month_year}')
    
    def launch_by_id(self, launch_id: str):
        return self.client.get(f'/launches/{launch_id}', content_type='application/json')
    
    def remove_by_id(self, launch_id: str):
        return self.client.delete(f'/launches/{launch_id}', content_type='application/json')
    
    def clear_all(self):
        return mongo.db.launch.delete_many({})
